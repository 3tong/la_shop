<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = "articles";
    protected $primaryKey = "art_id";
    public    $timestamps = false;
    protected $guarded = [];
	


}
