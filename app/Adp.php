<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adp extends Model
{
    protected $table = "ad_positions";
    protected $primaryKey = "adp_id";
    public $timestamps = false;
    protected $guarded = [];

}


