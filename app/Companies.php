<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $table = "companies";
    protected $primaryKey = "com_id";
    public 	  $timestamps = false;
    protected $guarded = [];
	
	/**
	 * 获取认证通过的企业
	 */
	public function getAll()
	{
		return Companies::where(['status'=>'0'])->get();
	}
}

