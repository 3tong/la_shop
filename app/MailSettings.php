<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailSettings extends Model
{
    protected $table = "mail_settings";
	protected $primaryKey = "id";
    public $timestamps = false;
    protected $guarded = [];

}


