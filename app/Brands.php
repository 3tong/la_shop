<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    protected $table = "brands";
    protected $primaryKey = "brand_id";
    public    $timestamps = false;
    protected $guarded = [];
	
	/**
	 * 获取全部品牌
	 */
	public function getAllbrands()
	{
		return $this::orderBy('brand_id','desc')->get();
	}

}
