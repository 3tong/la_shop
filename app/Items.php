<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = "items";
    protected $primaryKey = "item_id";
    public $timestamps = false;
    protected $guarded = [];
}
