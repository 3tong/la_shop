<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUsers extends Model
{
    protected $table = "admin_users";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $guarded = [];

}
