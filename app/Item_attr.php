<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_attr extends Model
{
    protected $table = "item_attr";
    protected $primaryKey = "attr_id";
    public $timestamps = false;
    protected $guarded = [];
}
