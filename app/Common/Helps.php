<?php
use	App\CommonSettings;
use	App\MailSettings;
use	App\Navs;
use App\Ad;
use App\Articles;

/**
 * 随机字符串
 */
if(!function_exists('strRand'))  
{  
    function strRand($num)  
    {  
        return substr(str_shuffle('QEUIOISDFDHJGKHJXVCBVCCMBNMNLKPLRTAWEASQWEASDZXCRFVTGBYHNUJMIKOLP'),0,$num);  
    }  
}  


/**
 * 截取网址
 */
if(!function_exists('getUrl'))  
{
	function getUrl($url)
	{
		if(str_contains($url, 'https://'))
		{
			return str_replace('https://', '', $url);			
		}
		else if(str_contains($url, 'http://'))
		{
			return str_replace('http://', '', $url);			
		}
	}
}	


/**
 * 时间戳转时间
 */
if(!function_exists('getTime'))  
{
	function getTime($time)
	{
		return date('Y-m-d',$time);
	}
}	


/**
 * 获取真实ip
 */
if(!function_exists('getIP'))  
{
	function getIP()
	{ 
		if (@$_SERVER["HTTP_X_FORWARDED_FOR"]) 
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"]; 
		else if (@$_SERVER["HTTP_CLIENT_IP"]) 
		$ip = $_SERVER["HTTP_CLIENT_IP"]; 
		else if (@$_SERVER["REMOTE_ADDR"]) 
		$ip = $_SERVER["REMOTE_ADDR"]; 
		else if (@getenv("HTTP_X_FORWARDED_FOR"))
		$ip = getenv("HTTP_X_FORWARDED_FOR"); 
		else if (@getenv("HTTP_CLIENT_IP")) 
		$ip = getenv("HTTP_CLIENT_IP"); 
		else if (@getenv("REMOTE_ADDR")) 
		$ip = getenv("REMOTE_ADDR"); 
		else 
		$ip = "Unknown"; 
		return $ip; 
	}
}	


/**
 * 获取设置项的值
 */
if(!function_exists('CS'))  
{
	function CS($item)
	{
		$val	= CommonSettings::select($item)
		->where('id',1)
		->first();
		return $val->$item;
	}
}	

/**
 *  处理上传的excel文件，并返回地址
 */
if(!function_exists('excelUpload'))  
{
	function excelUpload($type,$req)
	{
		$path = 'uploads/batchUpload/'.$type.'/';
		if(!is_dir($path))
		{
			mkdir($path,0777,true);
		}
		
		$file	=	$req->file('excel');	
		$extens	=	'.'.$file->getClientOriginalExtension();
		$name	=	$type.date('Ymdhis').$extens;
		$file->move($path,$name);
		return $path.$name;
	}
}

/**
 * 获取邮件设置项的值
 */
if(!function_exists('MS'))  
{
	function MS($item)
	{
		$val	= MailSettings::select($item)
		->where('id',1)
		->first();
		return $val->$item;
	}
}	

/**
 * 获取导航栏
 * @param	int  $len  需要的条数  
 */
if(!function_exists('getNav'))  
{
	function getNav($len = '')
	{
		if($len != '')
		{
			return Navs::where('is_show',0)
			->orderBy('sort_order','asc')
			->skip(0)->take($len)
			->get();
		}
		else
		{
			return Navs::where('is_show',0)
			->orderBy('sort_order','asc')
			->get();
		}
	}
}


/**
 * 获取某个广告位下的广告
 * @param  int  $id   广告位id
 * @param  int  $len  需要的条数
 */
if(!function_exists('getAd'))  
{
	function getAd($id,$len = '')
	{
		if($len != '')
		{
			return Ad::where('is_show',0)
				->where('adp_id',$id)
				->orderBy('sort_order','asc')
				->skip(0)->take($len)
				->get();
		}
		else
		{
			return Ad::where('is_show',0)
				->where('adp_id',$id)
				->orderBy('sort_order','asc')
				->get();		
		}
	}
}

/**
 * 获取某个栏目下的文章
 * @param  int  $id   文章分类id
 * @param  int  $len  需要的条数
 */
if(!function_exists('getArt'))  
{
	function getArt($id,$len = '')
	{
		if($len != '')
		{
			return Articles::where('is_show',0)
				->where('cat_id',$id)
				->orderBy('sort_order','asc')
				->skip(0)->take($len)
				->get();
		}
		else
		{
			return Articles::where('is_show',0)
				->where('cat_id',$id)
				->orderBy('sort_order','asc')
				->get();			
		}
	}	
}
