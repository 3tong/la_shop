<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommonSettings extends Model
{
    protected $table = "common_settings";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $guarded = [];

}


