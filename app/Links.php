<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    protected $table = "links";
    protected $primaryKey = "l_id";
    public $timestamps = false;
    protected $guarded = [];
}
