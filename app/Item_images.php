<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_images extends Model
{
    protected $table = "item_images";
    protected $primaryKey = "img_id";
    public $timestamps = false;
    protected $guarded = [];
}
