<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = "advertisements";
    protected $primaryKey = "ad_id";
    public $timestamps = false;
    protected $guarded = [];

}


