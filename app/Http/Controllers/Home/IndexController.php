<?php
namespace App\Http\Controllers\Home;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Articles;
use App\ArticleCates;

class IndexController extends Controller
{
    /**
	 * 前台首页
     */
    public function index(Request $req)
    {
		$title	= CS('site_name');
        return view('home.index.index',[
        	'title'			=> 	$title,
        	'navs'			=> 	getNav(),
        	'ad_under_nav'	=> 	getAd(1),
        	'helps'			=> 	getArt(4),
        	'tops'			=>  $this ->getTopArticles(1,27),
    	]);
    }
	
	
	
	/**
	 * 获取某个栏目下的置顶文章
	 * @param $id 	int 栏目id
	 * @param $len	int 条数
	 * return Array
	 */
	public function getTopArticles($id,$len=10)
	{
		$data = array();
		$child = (new ArticleCates)->getChild($id);

		$data = array();
		$child = (new ArticleCates)->getChild($id);
		foreach($child as $k => $v)
		{
			$res = ArticleCates::where('is_show',0)
				->where('cat_id',$v)
				->get()
				->toArray();
			$data = array_merge($data,$res);
		}
		
		foreach($data as $k => $v)
		{
			$res = Articles::orderBy('sort_order','asc')
				->where('is_show',0)
				->where('is_top',0)
				->where('cat_id',$v['cat_id'])
				->skip(0)->take($len)
				->get()
				->toArray();
			$data[$k]['tops'] = $res;
		}
		return $data;	
	} 
	
	

}
