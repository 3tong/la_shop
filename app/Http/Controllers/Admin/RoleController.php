<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\AdminActions;
use	App\AdminRoles;
use	App\AdminUsers;
use App\AdminRoleActions;

class RoleController extends Controller
{
	
	
		
	/**
	 * 设置分页大小
	 */
	public $pageSize = null;
	
	
	public function __construct()
	{
		$this->pageSize	 = CS('admin_page_size')!=''?CS('admin_page_size'):20;
	}



	public function index(Request $req)
	{
		//权限验证
		if(!in_array('role_list',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
		
		
		$title = trans('admin.role_list');
		$data = AdminRoles::paginate($this -> pageSize);
		return view('admin.role.role_list',['title'=>$title,
			'data' => $data,
		]);
	}

	
	/**
	 * 创建角色
	 */
	public function add(Request $req)
	{
		//权限验证
		if(!in_array('role_add',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
		
		if($req->isMethod('get'))
		{
			$acts =  (new AdminActions)->getMenu(0);
			$title = trans('admin.role_add');
			return view('admin.role.role_add',['title'=>$title,
				'acts' => $acts,
			]);
		}
		else if($req->isMethod('post'))
		{
			$input = Input::except('MAX_FILE_SIZE','_token');
			//验证字段
	        $rule = [
	            'role_name'	=> 'required|max:50',
	            'desc'		=> 'max:200',
	        ];
			
	        $message = [
	            'role_name.required'	=> '角色名称是必须的',
				'role_name.max' 		=> '角色名称长度不能大于50',	
	            'desc.max'				=> '角色描述长度不能大于200',
	        ];
			
	        $validate = Validator::make($input,$rule,$message);
	        if($validate->fails())
	        {
	            return back()-> withErrors($validate)->withInput($req->all());
	        }
			
			$role_id = AdminRoles::insertGetId(['role_name' => trim($req->role_name),
				'desc'	=> trim($req->desc),
			]);
			
			if(isset($input['act_id']))
			{
				foreach($input['act_id'] as $v)
				{
					AdminRoleActions::insert(['role_id' => $role_id,
						'act_id' => $v ,
					]);
				}
			}
			
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'succ',
				'info' => trans('admin.add_succ'),
				'url' => url('admin/role')
			]);
						
			
			
		}
	}














	/**
	 * 修改角色
	 */
	public function update(Request $req,$id)
	{
		//权限验证
		if(!in_array('role_edit',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
		
		if($req->isMethod('get'))
		{
			if(AdminRoles::where('role_id',$id)->count() < 1)
			{
				return redirect('admin/role');
			}
		
			$acts 	=  (new AdminActions)->getMenu(0);
			$title  = trans('admin.role_update');
			$actions =  array_flatten(json_decode(json_encode(AdminRoleActions::where('role_id',$id)->select('act_id')->get()),true));
			$role   = AdminRoles::where('role_id',$id)->first();

			return view('admin.role.role_update',['title'=>$title,
				'acts' => $acts,
				'role' => $role,
				'actions' => $actions,
			]);
		}
		else if($req->isMethod('post'))
		{
			$input = Input::except('MAX_FILE_SIZE','_token');
			//验证字段
	        $rule = [
	            'role_name'	=> 'required|max:50',
	            'desc'		=> 'max:200',
	        ];
			
	        $message = [
	            'role_name.required'	=> '角色名称是必须的',
				'role_name.max' 		=> '角色名称长度不能大于50',	
	            'desc.max'				=> '角色描述长度不能大于200',
	        ];
			
	        $validate = Validator::make($input,$rule,$message);
	        if($validate->fails())
	        {
	            return back()-> withErrors($validate)->withInput($req->all());
	        }
			
			AdminRoles::where('role_id',$id)
	            ->update(['role_name' => trim($req->role_name),
	            	'desc' => trim($req->desc),
	            ]);
				
			AdminRoleActions::where('role_id',$id)
				->delete();
				
			if(isset($input['act_id']))
			{
				foreach($input['act_id'] as $v)
				{
					AdminRoleActions::insert(['role_id' => $id,
						'act_id' => $v ,
					]);
				}
			}
			
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'succ',
				'info' => trans('admin.update_succ'),
				'url' => url('admin/role')
			]);
								

		}
	
	}

	
	/**
	 * 删除角色
	 */
	public function del(Request $req)
	{
		//权限验证
		if(!in_array('role_del',session('access')))
		{
			return ['error'=>1,
				'message'=>'fail',
				'content'=>'没有该操作的权限!',
			];
		}
		
		$id = isset($req->id)?intval($req->id):'0';
		if(AdminRoles::where('role_id',$id)->count() > 0)
		{
			if(AdminUsers::where('role_id',$id)->count() > 0)
			{
				return ['error'=>1,
					'message'=>'fail',
					'content'=>'已有管理员使用该角色，无法删除!',
				];
			}
			else
			{
				AdminRoles::where('role_id',$id)
					->delete();
				AdminRoleActions::where('role_id',$id)
					->delete();
				return ['error'=>0,
					'message'=>'success',
					'content'=>'删除成功!',
				];		
			}

		}

	}

















}
