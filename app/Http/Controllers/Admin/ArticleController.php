<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Articles;
use App\ArticleCates;
use Image;
use Excel;
use Redirect;

class ArticleController extends Controller
{
	public $cates  = null;
	
		
	/**
	 * 设置分页大小
	 */
	public $pageSize = null;
	
	
	public function __construct()
	{
		$this->cates	 = (new ArticleCates)->getTree((new ArticleCates)->getAllCates());
		$this->pageSize	 = CS('admin_page_size')!=''?CS('admin_page_size'):20;
	}



	/**
	 * 获取文章列表
	 */
	public function index(Request $req)
	{
		//权限验证
		if(!in_array('art_list',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',[
				'sec'	=>'3',
				'act'	=>'fail',
				'info' 	=> trans('admin.access_fail'),
				'url' 	=> url('admin/info')
			]);
		}
		
		$title 		= trans('admin.article_list');
		$art_id		= isset($req->art_id)?trim($req->art_id):'';
		$cat_id		= isset($req->cat_id)?trim($req->cat_id):'';
		$art_title	= isset($req->title)?trim($req->title):'';
		$bin		= Articles::where('is_show',1)->count();
		$where		= ['is_show' => 0];
		
		if($art_id !== '')
		{
			$where   =  array_merge($where, ['art_id' => $art_id]);
		}
		
		if( $title !== '' && $cat_id !== '')
		{
			$child = [$cat_id];
			$child = array_merge($child,(new ArticleCates)->getChild($cat_id));
			$data = Articles::orderBy('art_id','desc')
				->where($where)
				->whereIn('cat_id',$child)
				->where('title','like','%'.$art_title.'%')
            	->paginate($this -> pageSize);
		}
		else if($title !== '')
		{
			$data = Articles::orderBy('art_id','desc')
				->where($where)
				->where('title','like','%'.$art_title.'%')
            	->paginate($this -> pageSize);
		}
		else if($cat_id !== '')
		{
			$child = [$cat_id];
			$child = array_merge($child,(new ArticleCates)->getChild($cat_id));
			$data = Articles::orderBy('art_id','desc')
				->where($where)
				->whereIn('cat_id',$child)
            	->paginate($this -> pageSize);
		}
		else
		{
			$data = Articles::orderBy('art_id','desc')
				->where($where)
            	->paginate($this -> pageSize);	
		}
		
		
		return view('admin.article.article_list',['title'=>$title,
			'data'		=>	$data,
			'bin'		=> 	$bin,
			'cates'		=>	$this->cates,
			'art_id'	=>	$art_id,
			'cat_id'	=> 	$cat_id,
			'art_title'	=>  $art_title,
		]);
		
	}

	
	/**
	 * 文章添加
	 */
	public function add(Request $req)
	{
		
		//权限验证
		if(!in_array('art_add',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',[
				'sec'	=>'3',
				'act'	=>'fail',
				'info' 	=> trans('admin.access_fail'),
				'url' 	=> url('admin/info')
			]);
		}

		if($req->isMethod('get'))
		{
			$title  = trans('admin.article_add');
			return view('admin.article.article_add',['title'=>$title,
				'cates'		=>	$this->cates,

			]);			
		}
		else if($req->isMethod('post'))
		{
			$input = Input::except('MAX_FILE_SIZE','_token');
			//验证字段
	        $rule = [
	            'title' => 'required|max:150',
	            'desc'	=> 'max:255',
	            'cat_id'    => 'required|integer|not_in:0',
	            'click'    	=> 'integer',
	            'content'	=> 'max:10000',
	            'editor'	=> 'max:50',
	            'phone'		=> 'max:50',
	            'wechat'	=> 'max:50',
	            'qq'		=> 'max:50',
	            'is_hot'		  => 'required',
	            'is_top'		  => 'required',
	        ];
	        $message = [
	            'title.required' => '标题不能为空',
				'title.max' 	 => '标题长度不能大于100',
				'desc.max' 	 	 => '文章描述长度不能大于255',
				'content.max' 	 => '文章长度不能大于10000',
				'editor.max' 	 => '编辑长度不能大于50',
				'phone.max' 	 => '手机长度不能大于50',
				'wechat.max' 	 => '微信长度不能大于50',
				'qq.max' 	 => 'qq长度不能大于50',
				'cat_id.required' 	 => '请选择栏目',
				'cat_id.integer'	 => '栏目id为整型',
				'cat_id.not_in'		 => '栏目不能为顶级分类',
				'click.integer'	 	 => '点击次数为数字',
	            'is_top.required'      	=>'请选择是否置顶',
	            'is_hot.required'       =>'请选择是否热销',		
	        ];
	
	        $validate = Validator::make($input,$rule,$message);
	        if($validate->fails())
	        {
	            return back()-> withErrors($validate)->withInput($req->all());
	        }
			
			$time = $input['create_at']!==''?strtotime($input['create_at']):time();
			$art_id = Articles::insertGetId(['title' => trim($req->title),
				'desc'			=> trim($req->desc),
				'cat_id'		=> trim($req->cat_id),
				'content'		=> trim($req->content),
				'editor' 		=> trim($req->editor),
				'phone' 		=> trim($req->phone),
				'wechat' 		=> trim($req->wechat),
				'qq' 			=> trim($req->qq),
				'is_show' 		=> 0,
				'is_hot' 		=> trim($req->is_hot),
				'is_top' 		=> trim($req->is_top),
				'sort_order' 	=> trim($req->sort_order),
				'keyword' 		=> $this->getKeyWord($input),

				'click' 		=> trim($req->click),

				'create_at' 	=> $time,
				'update_at'  	=> $time,
			]);
			
			//文章图片处理
			if(isset($input['img']))
			{				
				$path = 'uploads/articles/'.date('Y/m/d/');
				if(!is_dir($path))
				{
					mkdir($path,0777,true);
				}
				
				$img_ori	= Image::make($input['img']);
		        $_oriPath  	= $path.rand(1111,9999).'_'.strRand(4).'.jpg';
				$img_ori->save($_oriPath);
				
				Articles::where('art_id',$art_id)
				->update(['img' => $_oriPath]);				
			}
			
			
			return redirect()->action('Admin\IndexController@tips',[
				'sec'	=>'3',
				'act'	=>'succ',
				'info' 	=> trans('admin.add_succ'),
				'url' 	=> url('admin/article')
			]);
						
		}
	}

	/**
	 * 回收站
	 */
	public	function bin(Request $req)
	{
		//权限验证
		if(!in_array('art_bin',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
		
		
		$title 		= trans('admin.article_bin');
		$art_id		= isset($req->art_id)?trim($req->art_id):'';
		$cat_id		= isset($req->cat_id)?trim($req->cat_id):'';
		$art_title	= isset($req->title)?trim($req->title):'';
		$bin		= Articles::where('is_show',1)->count();
		$where		= ['is_show' => 1];
		
		if($art_id !== '')
		{
			$where   =  array_merge($where, ['art_id' => $art_id]);
		}
		
		if( $title !== '' && $cat_id !== '')
		{
			$child = [$cat_id];
			$child = array_merge($child,(new ArticleCates)->getChild($cat_id));
			$data = Articles::orderBy('art_id','desc')
				->where($where)
				->whereIn('cat_id',$child)
				->where('title','like','%'.$art_title.'%')
            	->paginate($this -> pageSize);
		}
		else if($title !== '')
		{
			$data = Articles::orderBy('art_id','desc')
				->where($where)
				->where('title','like','%'.$art_title.'%')
            	->paginate($this -> pageSize);
		}
		else if($cat_id !== '')
		{
			$child = [$cat_id];
			$child = array_merge($child,(new ArticleCates)->getChild($cat_id));
			$data = Articles::orderBy('art_id','desc')
				->where($where)
				->whereIn('cat_id',$child)
            	->paginate($this -> pageSize);
		}
		else
		{
			$data = Articles::orderBy('art_id','desc')
				->where($where)
            	->paginate($this -> pageSize);	
		}
		
		
		return view('admin.article.article_bin',['title'=>$title,
			'data'		=>	$data,
			'bin'		=> 	$bin,
			'cates'		=>	$this->cates,
			'art_id'	=>	$art_id,
			'cat_id'	=> 	$cat_id,
			'art_title'	=>  $art_title,
		]);
				
	}
	





	/**
	 * 	文章修改
	 */
	public	function update(Request $req,$id)
	{
		//权限验证
		if(!in_array('art_edit',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
		
		if(Articles::where('art_id',$id)->count() < 1)
		{
			return redirect('admin/article');
		}

		$article	= articles::where('art_id',$id)->first();
		
		if($req->isMethod('get'))
		{
			$title 	= trans('admin.article_update');				
			return view('admin.article.article_update',['title'=>$title,
				'cates'		=>	$this->cates,
				'article'		=>		$article ,		
			]);
		}
		else if($req->isMethod('post'))
		{
			$input = Input::except('MAX_FILE_SIZE','_token');
			//验证字段
	        $rule = [
	            'title' => 'required|max:150',
	            'desc'	=> 'max:255',
	            'cat_id'    => 'required|integer|not_in:0',
	            'click'    	=> 'integer',
	            'content'	=> 'max:10000',
	            'editor'	=> 'max:50',
	            'phone'		=> 'max:50',
	            'wechat'	=> 'max:50',
	            'qq'		=> 'max:50',
	            'is_hot'		  => 'required',
	            'is_top'		  => 'required',
	        ];
			
	        $message = [
	            'title.required' => '标题不能为空',
				'title.max' 	 => '标题长度不能大于100',
				'desc.max' 	 	 => '文章描述长度不能大于255',
				'content.max' 	 => '文章长度不能大于10000',
				'editor.max' 	 => '编辑长度不能大于50',
				'phone.max' 	 => '手机长度不能大于50',
				'wechat.max' 	 => '微信长度不能大于50',
				'qq.max' 	 => 'qq长度不能大于50',
				'cat_id.required' 	 => '请选择栏目',
				'cat_id.integer'	 => '栏目id为整型',
				'cat_id.not_in'		 => '栏目不能为顶级分类',
				'click.integer'	 	 => '点击次数为数字',
	            'is_top.required'      	=>'请选择是否置顶',
	            'is_hot.required'       =>'请选择是否热销',		
	        ];
	
	        $validate = Validator::make($input,$rule,$message);
			
	        if($validate->fails())
	        {
	            return back()-> withErrors($validate)->withInput($req->all());
	        }
			
			$time = $input['create_at']!==''?strtotime($input['create_at']):time();
			Articles::where('art_id',$id)
			->update(['title' => trim($req->title),
				'desc'			=> trim($req->desc),
				'cat_id'		=> trim($req->cat_id),
				'content'		=> trim($req->content),
				'editor' 		=> trim($req->editor),
				'phone' 		=> trim($req->phone),
				'wechat' 		=> trim($req->wechat),
				'qq' 			=> trim($req->qq),
				'is_show' 		=> 0,
				'is_hot' 		=> trim($req->is_hot),
				'is_top' 		=> trim($req->is_top),
				'sort_order' 	=> trim($req->sort_order),
				'keyword' 		=> $this->getKeyWord($input),
				'click' 		=> trim($req->click),
				'create_at' 	=> $time,
				'update_at'  	=> $time,
			]);
			
			//文章图片处理
			if(isset($input['img']))
			{
				if($article -> img != '')
				{
					unlink(public_path($article -> img));
				}
				
								
				$path = 'uploads/articles/'.date('Y/m/d/');
				if(!is_dir($path))
				{
					mkdir($path,0777,true);
				}
				
				$img_ori	= Image::make($input['img']);
		        $_oriPath  	= $path.rand(1111,9999).'_'.strRand(4).'.jpg';
				$img_ori->save($_oriPath);
				
				Articles::where('art_id',$id)
				->update(['img' => $_oriPath]);				
			}

			
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'succ',
				'info' => trans('admin.update_succ'),
				'url' => url('admin/article'),
			]);			
		}	
	}



	/**
	 *	将文章放入回收站 	
	 */
	public function del($id)
	{
		//权限验证
		if(!in_array('art_del',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
	
		if(Articles::where('art_id',$id)->count() > 0)
		{
			Articles::where('art_id',$id)->update(['is_show' => 1]);
			return redirect('admin/article');
		}
	
	}
	
	
	/**
	 * 从回收站恢复文章
	 */
	public function articleRecover($id)
	{
		//权限验证
		if(!in_array('art_recover',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}

		if(Articles::where('art_id',$id)->count() > 0)
		{
			Articles::where('art_id',$id)->update(['is_show' => 0]);
			return redirect('admin/articleBin');
		}
		
	}
	
	
	/**
	 * 彻底删除文章
	 */
	public function	delTrue($id)
	{
		//权限验证
		if(!in_array('art_del_true',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}

		if(Articles::where('art_id',$id)->count() > 0)
		{
			$article = Articles::where('art_id',$id)->first();
			if($article -> img != '')
			{
				unlink(public_path($article -> img));
			}
			Articles::where('art_id',$id)->delete(['is_show' => 0]);
			return redirect('admin/articleBin');
		}		
	} 





	/**
	 * 文章分类列表
	 */
	public function cateList(Request $req)
	{
		//权限验证
		if(!in_array('art_cate_list',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}

		$title =  trans('admin.article_cate');
		
		return view('admin.article.cate_list',[
			'title'	=> $title,
			'data' => $this->cates,
		]);
		
	}


	/**
	 * 文章分类添加
	 */
	public function cateAdd(Request $req)
	{
		//权限验证
		if(!in_array('art_cate_add',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}

		if($req->isMethod('get'))
		{
			$title 	= trans('admin.cate_add');				
			return view('admin.article.cate_add',['title'=>$title,
				'cates'		=>	$this->cates,		
			]);
		}
		else if($req->isMethod('post'))
		{
			$input = Input::except('MAX_FILE_SIZE','_token');
			//验证字段
	        $rule = [
	            'parent_id'   => 'required|integer',
	            'cat_name'    => 'required|max:50',
				'is_show'	  => 'required|in:0,1',
				'sort_order'  => 'integer',
				'desc'		  => 'max:200',
	        ];
			
	        $message = [
	            'parent_id.required' 	=> '上级分类不能为空',
				'parent_id.integer'		=> '上级分类为整数',
				'cat_name.required'		=> '分类名称是必须的',
				'cat_name.max'			=> '分类名称长度不能大于50',
				'is_show.required'		=> '显示状态是必须的',
				'is_show.in'			=> '请选择“是”或“否”',
				'sort_order.integer'	=> '排序为整数',
				'desc.max'				=> '描述长度不能大于200',
				

	        ];
	
	        $validate = Validator::make($input,$rule,$message);
			
	        if($validate->fails())
	        {
	            return back()-> withErrors($validate)->withInput($req->all());
	        }
			
			ArticleCates::insert(['parent_id' => trim($req->parent_id),
				'cat_name'		=> trim($req->cat_name),
				'is_show' 		=> trim($req->is_show),
				'sort_order' 	=> trim($req->sort_order),
				'desc' 			=> trim($req->desc),
			]);
			
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'succ',
				'info' => trans('admin.add_succ'),
				'url' => url('admin/articleCate'),
			]);					
		}

	}

	/**
	 * 文章分类修改
	 */
	public function cateUpdate(Request $req,$id)
	{
		//权限验证
		if(!in_array('art_cate_edit',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
		
		//判断是分类否存在
		if(ArticleCates::where('cat_id',$id)->count() < 1)
		{
			return redirect('admin/artcleCate');
		}

		if($req->isMethod('get'))
		{
			$title 	= trans('admin.cate_add');	
			$cate	= ArticleCates::where('cat_id',$id)->first();			
			return view('admin.article.cate_update',['title'=>$title,
				'cates'		=>	$this->cates,
				'cate'		=>  $cate		
			]);
		}
		else if($req->isMethod('post'))
		{
			$input = Input::except('MAX_FILE_SIZE','_token');
			//验证字段
	        $rule = [
	            'parent_id'   => 'required|integer',
	            'cat_name'    => 'required|max:50',
				'is_show'	  => 'required|in:0,1',
				'sort_order'  => 'integer',
				'desc'		  => 'max:200',
	        ];
			
	        $message = [
	            'parent_id.required' 	=> '上级分类不能为空',
				'parent_id.integer'		=> '上级分类为整数',
				'cat_name.required'		=> '分类名称是必须的',
				'cat_name.max'			=> '分类名称长度不能大于50',
				'is_show.required'		=> '显示状态是必须的',
				'is_show.in'			=> '请选择“是”或“否”',
				'sort_order.integer'	=> '排序为整数',
				'desc.max'				=> '描述长度不能大于200',
				
	        ];
	
	        $validate = Validator::make($input,$rule,$message);
			
	        if($validate->fails())
	        {
	            return back()-> withErrors($validate)->withInput($req->all());
	        }
			
			ArticleCates::where('cat_id',$id)
			->update(['parent_id' => trim($req->parent_id),
				'cat_name'		=> trim($req->cat_name),
				'is_show' 		=> trim($req->is_show),
				'sort_order' 	=> trim($req->sort_order),
				'desc' 			=> trim($req->desc),
			]);
			
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'succ',
				'info' => trans('admin.update_succ'),
				'url' => url('admin/articleCate'),
			]);		
		}

	}
	
	
	/**
	 * 文章分类删除 
	 */
	public	function  cateDel(Request $req)
	{
		//权限验证
		if(!in_array('art_cate_del',session('access')))
		{
			return [
				'error'=>1,
				'message'=>'fail',
				'content'=>'没有该操作的权限！',
			];
		}
		
		//判断分类下是否有子级
		$cates = ArticleCates::where('parent_id',$req->id)->count();
		if($cates > 0)
		{
			return [
				'error'=>1,
				'message'=>'fail',
				'content'=>'该分类下还有子分类，请删除后再试！',
			];		
		}
		
		//判断分类下是否有文章
		$child = [$req->id];
		$child = array_merge($child,(new ArticleCates)->getChild($req->id));
		$count  = Articles::whereIn('cat_id',$child)->count();
		if($count > 0)
		{
			return [
				'error'=>1,
				'message'=>'fail',
				'content'=>'该分类下还有'.$count.'篇文章，请删除文章后再试！',
			];
		}
		
		if(ArticleCates::where('cat_id',$req->id)->count() > 0)
		{
			ArticleCates::where('cat_id',$req->id)->delete();
			return [
				'error'  =>0,
				'message'=>'success',
				'content'=>'删除成功！',
			];
		}
		

	}


	/**
	 * 生成文章关键字
	 */
	public	function  getKeyWord($input)
	{
		if($input['keyword'] !== '')
		{
			return trim($input['keyword']);
		}
		else
		{
			if($input['cat_id']!=0)
			{
				$cat = ArticleCates::where('cat_id',$input['cat_id'])->select('cat_name')->first()->cat_name.(new ArticleCates)->getParentKeyword($input['cat_id']);
			}
			else
			{
				$cat = '';
			}
			
			return trim($input['title']).' '.$cat;
		}
	}




	/**
	 *	文章批量操作 
	 */
	public function articleHandle(Request $req)
	{
		//批量设为推荐
		if($req->act == 'hot')
		{
			$in = array_flatten($req->items);
			Articles::whereIn('art_id',$in)
            ->update(['is_hot' => 0]);
			return ['error'=>0,'message'=>'success'];
		}
		
		//批量取消推荐
		if($req->act == 'unhot')
		{
			$in = array_flatten($req->items);
			Articles::whereIn('art_id',$in)
            ->update(['is_hot' => 1]);
			return ['error'=>0,'message'=>'success'];
		}
		
		//批量设为推荐
		if($req->act == 'top')
		{
			$in = array_flatten($req->items);
			Articles::whereIn('art_id',$in)
            ->update(['is_top' => 0]);
			return ['error'=>0,'message'=>'success'];
		}
		
		
		//批量设为推荐
		if($req->act == 'untop')
		{
			$in = array_flatten($req->items);
			Articles::whereIn('art_id',$in)
            ->update(['is_top' => 1]);
			return ['error'=>0,'message'=>'success'];
		}
		
		
		//批量加入回收站
		if($req->act == 'del')
		{
			$in = array_flatten($req->items);
			Articles::whereIn('art_id',$in)
            ->update(['is_show' => 1]);
			return ['error'=>0,'message'=>'success'];
		}
		
		//批量删除
		if($req->act == 'del_true')
		{
			$in = array_flatten($req->items);
			
			$arts = Articles::whereIn('art_id',$in)->get();
			foreach($arts as $k => $v)
			{
				if($v->img !== '')
				{
					unlink(public_path($v -> img));				
				}

			}
			Articles::whereIn('art_id',$in)
            ->delete();

			return ['error'=>0,'message'=>'success'];
		}
		
		
		//批量从回收站恢复
		if($req->act == 'recover')
		{
			$in = array_flatten($req->items);
			Articles::whereIn('art_id',$in)
            ->update(['is_show' => 0]);
			return ['error'=>0,'message'=>'success'];
		}


	}



	/**
	 * 文章批量上传
	 */
	public function batchUpload(Request $req)
	{		
		//权限验证
		if(!in_array('art_import',session('access')))
		{
			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'fail',
				'info' => trans('admin.access_fail'),
				'url' => url('admin/info')
			]);
		}
		
		if($req->isMethod('get'))
		{
			$title = trans('admin.article_batch_upload');
			return view('admin.article.batch',[
				'title' 	=> 	$title,
				'cates'		=>	$this->cates,	
			]);
		}
		else if($req->isMethod('post'))
		{
			$input = Input::except('MAX_FILE_SIZE','_token');
			//验证字段
	        $rule = [
	         	'cat_id'    => 'required|integer|not_in:0',
	            'excel'   => 'required',
	        ];
			
	        $message = [
				'cat_id.required' 	 => '请选择栏目',
				'cat_id.integer'	 => '栏目id为整型',
				'cat_id.not_in'		 => '栏目不能为顶级分类',
	            'excel.required' 	 => '请上传表格',
	        ];
	
	        $validate = Validator::make($input,$rule,$message);
			
	        if($validate->fails())
	        {
	            return back()-> withErrors($validate)->withInput($req->all());
	        }
			
		
			//处理上传excel文件并返回地址
			$file = excelUpload('articles',$req);

			$res = Excel::load($file, function($reader){})->toArray();
			
			foreach($res as $k => $v)
			{
				$time = !empty($v[10])?strtotime($v[10]):time();
				$input['title'] = trim($v[0]);
				$input['keyword'] = '';
				Articles::insert([
					'title'			=> trim($v[0]),
					'desc'			=> trim(str_limit($v[1],150)),
					'cat_id'		=> trim($req->cat_id),
					'content'		=> trim($v[1]),
					'editor' 		=> trim($v[2]),
					'phone' 		=> trim($v[3]),
					'wechat' 		=> trim($v[4]),
					'qq' 			=> trim($v[5]),
					'is_show' 		=> 0,
					'is_hot' 		=> trim($v[6]),
					'is_top' 		=> trim($v[7]),
					'sort_order' 	=> trim($v[9]),
					'keyword' 		=> $this->getKeyWord($input),
					'click' 		=> trim($v[8]),
					'create_at' 	=> $time,
					'update_at'  	=> $time,
				]);	
			}
  			return redirect()->action('Admin\IndexController@tips',['sec'=>'3','act'=>'succ',
				'info' => trans('admin.import_succ'),
				'url' => url('admin/article'),
			]);		
			
		}		
	}












}
