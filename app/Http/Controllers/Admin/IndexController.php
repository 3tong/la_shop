<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Items;
use App\Companies;
use App\Ad;
use	App\Articles;
class IndexController extends Controller
{
	/**
	 * 后台入口方法
	 */
    public function index()
	{	
    	$title = trans('admin.admin');
        return view('admin.index.index',['title'=>$title]);
    }
	
	/**
	 * 展示服务器信息
	 */
	public function info()
	{
		$title = trans('admin.server');
		return view('admin.index.info',[
			'items'			=>  Items::count(),
			'companies'		=>  Companies::count(),
			'articles'		=>  Articles::count(),
			'ads'			=> 	Ad::count(),
			'title'			=>	$title,
			'ser'			=>	$_SERVER,
			'run'			=>	php_sapi_name(),
		]);
	}
	
	
	/**
	 * 提示信息
	 */
	public function tips(Request $req)
	{
		$title = trans('admin.tips');
		return view('admin.index.tips',['title'=> $title,
			'act'	=>	$req->act,
			'info'	=>	$req->info,
			'url'	=>	$req->url,
			'sec'	=>	$req->sec,
		]);
	}
	
	

}
