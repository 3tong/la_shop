<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['namespace'=>'Home'],function(){
	//前台首页
	Route::any('/','IndexController@index');
	//文章列表
	Route::get('/articlecate/{id}','ArticleController@index');
	//文章详情
	Route::get('/article/{id}','ArticleController@detail');
	
	
});


/**
 * 后台路由
 */
Route::group(['namespace'=>'Admin','prefix'=>'admin','middleware'=>['App\Http\Middleware\Login']],function(){
	
	
	//后台首页
	Route::get('/','IndexController@index');
	//服务器信息
	Route::get('/info','IndexController@info');
	//提示信息
	Route::get('/tips','IndexController@tips');

	//角色列表
	Route::get('/role','RoleController@index');
	//添加角色
	Route::any('/roleAdd','RoleController@add');
	//修改角色
	Route::any('/roleUpdate/{id}','RoleController@update');
	//删除角色接口
	Route::post('/roleDel','RoleController@del');
		
	//管理员列表
	Route::get('/admin','AdminController@index');
	//管理员添加
	Route::any('/adminAdd','AdminController@add');	
	//管理员修改
	Route::any('/adminUpdate/{id}','AdminController@update');
	//删除管理员接口
	Route::post('/adminDel','AdminController@del');
	
	//公司列表
	Route::get('/company','CompanyController@index');
	//公司修改
	Route::any('/companyUpdate/{id}','CompanyController@update');
	//公司批量操作接口
	Route::post('/companyHandle','CompanyController@companyHandle');
	
	//商品分类
	Route::get('/itemCate','ItemController@cateList');
	//商品分类添加
	Route::any('/itemCateAdd','ItemController@cateAdd');
	//商品分类修改
	Route::any('/itemCateUpdate/{id}','ItemController@cateUpdate');
	//商品分类删除
	Route::any('/itemCateDel','ItemController@cateDel');

	//商品列表
	Route::get('/item','ItemController@index');
	//回收站列表
	Route::get('/itemBin','ItemController@bin');
	//商品添加
	Route::any('/itemAdd','ItemController@add');
	//修改商品
	Route::any('/itemUpdate/{id}','ItemController@update');
	//删除商品
	Route::any('/itemDelTrue/{id}','ItemController@itemDelTrue');
	//商品加入回收站
	Route::get('/itemDel/{id}','ItemController@itemDel');
	//商品从回收站恢复
	Route::get('/itemRecover/{id}','ItemController@itemRecover');
	//查询品牌下的型号接口 
	Route::post('/getModel','BrandController@getModelByBrand');
	//查询指定省份下的市
	Route::post('/getCity','ItemController@getCityByProvince');
	//查询指定市下的区县
	Route::post('/getArea','ItemController@getAreaByCity');
	//商品批量操作接口
	Route::post('/itemHandle','ItemController@itemHandle');
	//删除商品单个图片接口
	Route::post('/imgDel','ItemController@imgDel');
	//修改商品单个图片描述接口
	Route::post('/imgDescUpdate','ItemController@imgDescUpdate');
	//删除商品单条属性接口
	Route::post('/attrDel','ItemController@attrDel');
	//修改商品单个属性
	Route::post('/attrUpdate','ItemController@attrUpdate');
	
	
	//品牌列表
	Route::get('/brand','BrandController@index');
	//添加品牌/型号
	Route::any('/brandAdd','BrandController@brandAdd');
	//修改品牌/型号
	Route::any('/brandUpdate/{id}','BrandController@brandUpdate');
	//型号修改接口
	Route::post('/modelUpdate','BrandController@modelUpdate');
	//品牌删除接口
	Route::post('/brandDel','BrandController@del');
	
	//品牌批量操作接口
	Route::post('/brandHandle','BrandController@brandHandle');
	
	
	
	
	//文章列表
	Route::get('/article','ArticleController@index');
	//添加文章
	Route::any('/articleAdd','ArticleController@add');
	//修改文章
	Route::any('/articleUpdate/{id}','ArticleController@update');
	//文章加入回收站
	Route::get('/articleDel/{id}','ArticleController@del');
	//文章删除
	Route::get('/articleDelTrue/{id}','ArticleController@delTrue');
	//回收站
	Route::get('/articleBin','ArticleController@bin');
	//从回收站恢复
	Route::get('/articleRecover/{id}','ArticleController@articleRecover');
	//文章批量操作
	Route::post('/articleHandle','ArticleController@articleHandle');
	//文章分类
	Route::get('/articleCate','ArticleController@cateList');
	//文章分类添加
	Route::any('/articleCateAdd','ArticleController@cateAdd');
	//文章分类修改
	Route::any('/articleCateUpdate/{id}','ArticleController@cateUpdate');
	//文章分类删除
	Route::any('/articleCateDel','ArticleController@cateDel');
	
	//广告位置列表
	Route::get('/adp','AdvertisementController@adp');
	//广告位置添加
	Route::any('/adpAdd','AdvertisementController@adpAdd');
	//广告位置修改
	Route::any('/adpUpdate/{id}','AdvertisementController@adpUpdate');
	//广告位置删除
	Route::any('/adpDel','AdvertisementController@adpDel');
	
	//广告列表
	Route::get('/ad','AdvertisementController@ad');	
	//广告添加
	Route::any('/adAdd','AdvertisementController@adAdd');
	//广告修改
	Route::any('/adUpdate/{id}','AdvertisementController@adUpdate');
	//广告删除
	Route::get('/adDel/{id}','AdvertisementController@adDel');
	
	
	
	
	//系统设置
	Route::any('/commonSetting','SettingController@index');
	//邮件设置
	Route::any('/emailSetting','SettingController@email');
	//邮件测试
	Route::any('/emailTest','SettingController@emailTest');
	//列表导航栏
	Route::get('/nav','SettingController@nav');
	//添加导航栏
	Route::any('/navAdd','SettingController@navAdd');
	//修改导航栏
	Route::any('/navUpdate/{id}','SettingController@navUpdate');
	//删除导航栏
	Route::get('/navDel/{id}','SettingController@navDel');
	
	//友情链接列表
	Route::get('/link','SettingController@link');
	//友情链接添加
	Route::any('/linkAdd','SettingController@linkAdd');
	//友情链接修改
	Route::any('/linkUpdate/{id}','SettingController@linkUpdate');
	//友情链接删除
	Route::get('/linkDel/{id}','SettingController@linkDel');
	
	
	
	//商品批量导入
	Route::any('/itemBatchUpload','ItemController@batchUpload');
	//企业批量导入
	Route::any('/companyBatchUpload','CompanyController@batchUpload');
	//文章批量导入
	Route::any('/articleBatchUpload','ArticleController@batchUpload');
	
});

Route::group(['namespace'=>'Admin','prefix'=>'admin'],function(){
	//管理员登录
	Route::any('/login','AdminController@login');
	Route::any('/logout','AdminController@logout');
});