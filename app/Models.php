<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    protected $table = "models";
    protected $primaryKey = "model_id";
    public    $timestamps = false;
    protected $guarded = [];
	
	
	/**
	 * 获取全部型号
	 */
	public function getAllModels()
	{
		return Models::where('is_show',0)->get();
	}
	

	
}