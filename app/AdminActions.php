<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminActions extends Model
{
    protected $table = "admin_actions";
    protected $primaryKey = "act_id";
    public $timestamps = false;
    protected $guarded = [];



   	/**
	 * 获取无限级菜单
	 */
    public function getTree($all, $parent_id = 0, $level = 0, $html = '--')
    {
        $tree = array();
        foreach ($all as $v)
	 	{
            if ($v->parent_id == $parent_id) 
            {
                $v->level = $level + 1;
                $v->html = str_repeat($html, $level);
                $tree[] = $v;
                $tree = array_merge($tree, $this->getTree($all, $v->act_id, $level + 1, $html));
            }
        }
        return $tree;
    }
	

	/**
	 * 获取操作的所有子级
	 */
	public	function getChild($act_id)
	{
        $tree = [];
        foreach ($this::all() as $v)
	 	{
            if($v->parent_id == $act_id) 
            {
                $tree[] = $v->act_id;
                $tree = array_merge($tree, $this->getChild($v->act_id));
            }
        }
        return $tree;
	}
	
	
	/**
	 * 获取菜单
	 */
	public function getMenu($parent_id = 0)
	{
		$menu = [];
		$all  = $this::where('parent_id',$parent_id)->get();

		foreach( $all as $k => $v)
		{
			$menu[$k] = $v;
			$menu[$k]['list'] = $this::where('parent_id',$v->act_id)->get();
		}
		return $menu;
	}  














}


