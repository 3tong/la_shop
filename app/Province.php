<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "province";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $guarded = [];

}
