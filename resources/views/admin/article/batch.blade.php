@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/role')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				
				<div class="row">
					<label>文章分类</label>
					<div class="rl">
						<select name="cat_id">
							<option value="0">顶级分类</option>
							@foreach($cates as $k=>$v)
								<option @if(old('cat_id') == $v->cat_id) selected="selected" @endif value="{{$v->cat_id}}">{{str_repeat('&emsp;',$v->level)}}{{$v->cat_name}}</option>
							@endforeach
						</select>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>选择文件</label>
					<div class="rl">
						<li><input type="file" class="bn excel" name="excel" <b>*</b></li>
					</div>
				</div>
				
				
				<div class="row">
					<label></label>
					<div class="rl">
						<li><a href="{{asset('/models/article.xlsx')}}">下载Excel模板</a></li>
					</div>
				</div>
				
				<div class="row">
					<label>使用说明</label>
					<div class="rl">
							<li>1. 碰到“是否推荐”之类，填写数字 0 或者 1，1 代表“ 否”，0 代表“ 是”。</li>
							<li>2. 选择上传文章的分类</li>
							<li>3. 上传excel格式为csv，xls，xlsx且只能按照模板所提供的格式填写。</li>
							<li>4. 单个excel文件上传条数不建议大于1000。</li>
					</div>
				</div>
				
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
				

						
			</form>			
		</div>


		
	</body>

</html>

