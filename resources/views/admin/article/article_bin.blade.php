@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/article')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>

			<form action="" method="get" class="search">
				<p id="warning" style="display: none">Tip:<b id="#info"></b></p>
				<li><b>编号&nbsp;:&nbsp;</b><input type="text" name="art_id" value="{{$art_id}}"/></li>
				<li>
					<b>分类&nbsp;:&nbsp;</b>
					<select name="cat_id">
						<option value="">全部</option>
						@foreach($cates as $k=>$v)
							<option value="{{$v->cat_id}}" @if($v->cat_id == $cat_id)selected="selected"@endif>{{str_repeat('&emsp;',$v->level)}}{{$v->cat_name}}</option>
						@endforeach
					</select>
				</li>
				<li><b>标题&nbsp;:&nbsp;</b><input type="text" name="title"  value="{{$art_title}}"/></li>
				<li><input type="submit" value="查询" class="submit"/></li>
			</form>
			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>选择</th>
						<th>编号</th>
						<th>文章标题</th>
						<th>推荐</th>
						<th>置顶</th>
						<th>排序</th>
						<th>操作</th>
					</tr>
					@forelse($data as $a)
					<tr>
						<td><input type="checkbox" id="{{$a->art_id}}"/></td>
						<td><i class="num">{{$a->art_id}}</i></td>
						<td><a href="">{{$a->title}}</a></td>
						<td>
							@if($a->is_hot == '0')<font class="green">是</font>
							@elseif($a->is_hot == '1')<font class="red">否</font>
							@endif
						</td>
						<td>
							@if($a->is_top == '0')<font class="green">是</font>
							@elseif($a->is_top == '1')<font class="red">否</font>
							@endif
						</td>
						<td><i class="num">{{$a->sort_order}}</i></td>
						<td><a class="fa fa-refresh" aria-hidden="true" href="{{url('admin/articleRecover',[$a->art_id])}}">恢复</a>|<a  class="fa fa-times" aria-hidden="true" onclick="if(confirm('确定将该文章彻底删除吗?这步操作将无法恢复。')){window.location.href='{{url('admin/articleDelTrue',[$a->art_id])}}';}">彻底删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>



		</div>
		
		<div class="page_box">
			@if($data != '' || $data->count()>0)
			<div class="handle_box">
				<input type="checkbox" id="all"/><font>全选</font>
				<button disabled="disabled" id="del_true"><i class="fa fa-trash" aria-hidden="true"></i>彻底删除</button>
				<button disabled="disabled" id="recover"><i class="fa fa-refresh" aria-hidden="true"></i>恢复</button>
			</div>
			@endif
			@if($data != '')
			<div class="pager f_r">
				{!!$data->appends(['art_id'	=>	$art_id,
					'cat_id' => $cat_id,
					'title'	 => $art_title, 
				])->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="hidden" name="art_id" value="{{$art_id}}"/>
					<input type="hidden" name="cat_id" value="{{$cat_id}}"/>
					<input type="hidden" name="title"  value="{{$art_title}}"/>		
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	




	</body>
	
	<script>
		$('#all').on('change',function(){
			var	tthis = $(this);
			var list = $('.main_list .list td input');
			if(tthis.is(':checked'))
			{
				list.attr('checked','checked');
				$('#del_true').removeAttr('disabled');
				$('#recover').removeAttr('disabled');
			}
			else
			{
				$('#del_true').attr('disabled','disabled');
				$('#recover').attr('disabled','disabled');
				list.removeAttr('checked');
			}
		});
		
		$('.main_list .list td input').on('change',function(){
			if($('.main_list .list td input:checked').length > 0)
			{
				$('#del_true').removeAttr('disabled');
				$('#recover').removeAttr('disabled');
			}
			else
			{
				$('#del_true').attr('disabled','disabled');
				$('#recover').attr('disabled','disabled');
				$('#all').removeAttr('checked');
			}
		});
		
		$('#del_true,#recover').click(function(){
			var list 	= $('.main_list .list td input:checked');
			var items 	= new Array();
			var act		= $(this).attr('id');  
			list.each(function(){  
				items.push($(this).attr('id'));
		   	});
		   	if(confirm('确定对这 '+items.length+' 项进行操作吗?'))
			{
				var url		= '{{action('Admin\ArticleController@articleHandle')}}';
				var data	= {'items':items,'act':act,'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					console.log(res);
					if(res.error == 0)
					{
						window.location.reload();
					}
				},'json');
			}
		});
	</script>
</html>
