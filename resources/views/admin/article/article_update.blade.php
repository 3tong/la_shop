@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/article')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>文章分类</label>
					<div class="rl">
						<select name="cat_id">
							<option value="0">顶级分类</option>
							@foreach($cates as $k=>$v)
								<option @if($article -> cat_id == $v->cat_id) selected="selected" @endif value="{{$v->cat_id}}">{{str_repeat('&emsp;',$v->level)}}{{$v->cat_name}}</option>
							@endforeach
						</select>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>文章标题</label>
					<div class="rl">
						<input class="artitle" type="text" name="title"  value="{{$article -> title}}"/>
						<b>*</b>	
					</div>
				</div>	
				
				<div class="row">
					<label>文章简介</label>
					<div class="rl">
						<textarea name="desc" class="desc">{{$article -> desc}}</textarea>
					</div>
				</div>	
							
				<div class="row">
					<label>文章图片</label>
					<div class="rl" id="imglist">
						@if($article->img != '')
						<div>
							<div class="img_box">
								<img id="b_l_l" src="{{asset($article->img)}}" style="width:120px; height:90px;"/>
							</div>
						</div>
						@endif								
						<li><input type="file" class="bn file" name="img" value=""/></li>
					</div>
				</div>
				
				
				<div class="row">
					<label>文章内容</label>
					<div class="rl">
						<script id="container" name="content" type="text/plain">{!! $article -> content !!}</script>	
					</div>
				</div>
				
				<div class="row">
					<label>编辑</label>
					<div class="rl">
						<input type="text" name="editor" value="{{$article -> editor}}"/>
					</div>
				</div>
				
				<div class="row">
					<label>手机</label>
					<div class="rl">
						<input type="text" name="phone" value="{{$article -> phone}}"/>
					</div>
				</div>
				

				<div class="row">
					<label>微信</label>
					<div class="rl">
						<input type="text" name="wechat" value="{{$article -> wechat}}"/>
					</div>
				</div>
				

				<div class="row">
					<label>QQ</label>
					<div class="rl">
						<input type="text" name="qq" value="{{$article -> qq}}"/>
					</div>
				</div>
				
				
				
				<div class="row">
					<label>发布日期</label>
					<div class="rl">
						<input placeholder="请输入日期" value="{{getTime($article->create_at)}}" name="create_at" class="laydate-icon" onclick="laydate()">
					</div>
				</div>	
				



				<div class="row">
					<label>推荐</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_hot" @if($article->is_hot =='0') checked="checked" @endif value="0" /><font>是</font>
						<input type="radio" class="cb" name="is_hot" @if($article->is_hot =='1') checked="checked" @endif value="1" /><font>否</font>
					</div>
				</div>	

				<div class="row">
					<label>置顶</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_top" @if($article->is_top =='0') checked="checked" @endif value="0" /><font>是</font>
						<input type="radio" class="cb" name="is_top" @if($article->is_top =='1') checked="checked" @endif value="1" /><font>否</font>
					</div>
				</div>	
				
				
				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" name="sort_order" value="{{$article->sort_order}}"/>
					</div>
				</div>


	
				
				<div class="row">
					<label>点击次数</label>
					<div class="rl">
						<input type="text" name="click" value="{{$article->click}}" />	
					</div>
				</div>	
				
				<div class="row">
					<label>关键字</label>
					<div class="rl">
						<input type="text" class="kw" value="{{$article->keyword}}" name="keyword"/>
						<b>如改动文章信息建议删除原关键字，系统将重新生成。</b>	
					</div>
				</div>
				
							
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>
				

		<script>
		    var ue = UE.getEditor('container');
			//实例化编辑器
		    ue.ready(function() {
		       ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
		    });
		    
		    $('.main_form .row .rl div .img_box img').click(function(){
		    	if($(this).css('width') == '120px')
		    	{
		    		$(this).css({'width':'320','height':'240'});	
		    	}
		    	else
		    	{
		    		$(this).css({'width':'120','height':'90'});
		    	}
				
		    });
		</script>
		
	</body>

</html>

