@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/articleAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.article_add')}}</a>
				<a class="bin" href="{{url('admin/articleBin')}}"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;{{trans('admin.bin')}}<b>{{$bin}}</b></a>
			</h3>

			<form action="" method="get" class="search">
				<p id="warning" style="display: none">Tip:<b id="#info"></b></p>
				<li><b>编号&nbsp;:&nbsp;</b><input type="text" name="art_id" value="{{$art_id}}"/></li>
				<li>
					<b>分类&nbsp;:&nbsp;</b>
					<select name="cat_id">
						<option value="">全部</option>
						@foreach($cates as $k=>$v)
							<option value="{{$v->cat_id}}" @if($v->cat_id == $cat_id)selected="selected"@endif>{{str_repeat('&emsp;',$v->level)}}{{$v->cat_name}}</option>
						@endforeach
					</select>
				</li>
				<li><b>标题&nbsp;:&nbsp;</b><input type="text" name="title"  value="{{$art_title}}"/></li>
				<li><input type="submit" value="查询" class="submit"/></li>
			</form>
			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>选择</th>
						<th>编号</th>
						<th>文章标题</th>
						<th>推荐</th>
						<th>置顶</th>
						<th>排序</th>
						<th>操作</th>
					</tr>
					@forelse($data as $a)
					<tr>
						<td><input type="checkbox" id="{{$a->art_id}}"/></td>
						<td><i class="num">{{$a->art_id}}</i></td>
						<td><a target="_blank" href="{{url('/article',[$a->art_id])}}">{{$a->title}}</a></td>
						<td>
							@if($a->is_hot == '0')<font class="green">是</font>
							@elseif($a->is_hot == '1')<font class="red">否</font>
							@endif
						</td>
						<td>
							@if($a->is_top == '0')<font class="green">是</font>
							@elseif($a->is_top == '1')<font class="red">否</font>
							@endif
						</td>
						<td><i class="num">{{$a->sort_order}}</i></td>
						<td><a target="_blank" href="{{url('/article',[$a->art_id])}}" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/articleUpdate',[$a->art_id])}}">编辑</a>|<a class="fa fa-times" aria-hidden="true" onclick="if(confirm('确定将文章放入回收站吗？')){window.location.href='{{url('admin/articleDel',[$a->art_id])}}';}">删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '' || $data->count()>0)
			<div class="handle_box">
				<input type="checkbox" id="all"/><font>全选</font>
				<button disabled="disabled" id="del"><i class="fa fa-trash" aria-hidden="true"></i>回收站</button>
				<button disabled="disabled" id="hot"><i class="fa fa-check" aria-hidden="true"></i>设置推荐</button>
				<button disabled="disabled" id="unhot"><i class="fa fa-times" aria-hidden="true"></i>取消推荐</button>
				<button disabled="disabled" id="top"><i class="fa fa-check" aria-hidden="true"></i>置顶</button>
				<button disabled="disabled" id="untop"><i class="fa fa-times" aria-hidden="true"></i>取消置顶</button>
			</div>
			@endif
			@if($data != '')
			<div class="pager f_r">
				{!!$data->appends(['art_id'	=>	$art_id,
					'cat_id' => $cat_id,
					'title'	 => $art_title, 
				])->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="hidden" name="art_id" value="{{$art_id}}"/>
					<input type="hidden" name="cat_id" value="{{$cat_id}}"/>
					<input type="hidden" name="title"  value="{{$art_title}}"/>		
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	




	</body>
	
	<script>
		$('#all').on('change',function(){
			var	tthis = $(this);
			var list = $('.main_list .list td input');
			if(tthis.is(':checked'))
			{
				list.attr('checked','checked');
				$('#top').removeAttr('disabled');
				$('#untop').removeAttr('disabled');
				$('#del').removeAttr('disabled');
				$('#hot').removeAttr('disabled');
				$('#unhot').removeAttr('disabled');
			}
			else
			{
				$('#top').attr('disabled','disabled');
				$('#untop').attr('disabled','disabled');
				$('#del').attr('disabled','disabled');
				$('#hot').attr('disabled','disabled');
				$('#unhot').attr('disabled','disabled');
				list.removeAttr('checked');
			}
		});
		
		$('.main_list .list td input').on('change',function(){
			if($('.main_list .list td input:checked').length > 0)
			{				
				$('#top').removeAttr('disabled');
				$('#untop').removeAttr('disabled');
				$('#del').removeAttr('disabled');
				$('#hot').removeAttr('disabled');
				$('#unhot').removeAttr('disabled');
			}
			else
			{
				$('#top').attr('disabled','disabled');
				$('#untop').attr('disabled','disabled');
				$('#del').attr('disabled','disabled');
				$('#hot').attr('disabled','disabled');
				$('#unhot').attr('disabled','disabled');
				$('#all').removeAttr('checked');
			}
		});
		
		
		$('#hot,#unhot,#del,#top,#untop').click(function(){
			var list 	= $('.main_list .list td input:checked');
			var items 	= new Array();
			var act		= $(this).attr('id');  
			list.each(function(){  
				items.push($(this).attr('id'));
		   	});
		   	if(confirm('确定对这 '+items.length+' 项进行操作吗?'))
			{
				var url		= '{{action('Admin\ArticleController@articleHandle')}}';
				var data	= {'items':items,'act':act,'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
				},'json');
			}
		});
	</script>
</html>
