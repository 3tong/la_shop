@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
			</h3>
			<form action="" method="get" class="search">
				<p id="warning" style="display: none">Tip:<b id="#info"></b></p>
				<li><b>编号&nbsp;:&nbsp;</b><input type="text" name="com_id" id="com_id" value="{{$com_id}}"/></li>
				<li><b>企业名称&nbsp;:&nbsp;</b><input type="text" name="com_name" value="{{$com_name}}"/></li>
				<li><b>类型&nbsp;:&nbsp;</b>
				<select name="type">
					<option value=""  @if($type == '') selected="selected" @endif>全部</option>
					<option value="1" @if($type == '1') selected="selected" @endif>采购商</option>
					<option value="0" @if($type == '0') selected="selected" @endif>供应商</option>
				</select>
				</li>				
				<li><b>状态&nbsp;:&nbsp;</b>
				<select name="status">
					<option value=""  @if($status == '') selected="selected" @endif>全部</option>
					<option value="1" @if($status == '1') selected="selected" @endif>未认证</option>
					<option value="0" @if($status == '0') selected="selected" @endif>已认证</option>
				</select>
				</li>
				<li><input type="submit" value="查询" class="submit"/></li>
			</form>
			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr><th>选择</th><th>编号</th><th>企业名称</th><th>类型</th><th>认证状态</th><th>联系人</th><th>联系电话</th><th>操作</th></tr>
					@forelse($data as $c)
					<tr>
						<td><input type="checkbox" id="{{$c->com_id}}"/></td>
						<td><i class="num">{{$c->com_id}}</i></td>
						<td><a href="">{{$c->com_name}}</a></td>
						<td>@if($c->type == '0') 供应商  @elseif($c->type == '1') 采购商   @endif</td>
						<td><span class="@if($c->status == '0') rz @elseif($c->status == '1') wrz @endif"></span></td>
						<td>{{$c->linkman}}</td>
						<td>{{$c->telephone}}</td>
						<td><a href="" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/companyUpdate',[$c->com_id])}}">编辑</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		

		<div class="page_box">
			@if($data != '' || $data->count()>0)
			<div class="handle_box">
				<input type="checkbox" id="all"/><font>全选</font>
				<button disabled="disabled" id="pass"><i class="fa fa-check" aria-hidden="true"></i>通过认证</button>
				<button disabled="disabled" id="unpass"><i class="fa fa-times" aria-hidden="true"></i>取消认证</button>
			</div>
			@endif
			@if($data != '')
			<div class="pager f_r">
				{!!$data->appends(['com_id' => $com_id,'com_name'=> $com_name,'type'=> $type,'status'=>$status])->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="hidden" name="com_id" value="{{$com_id}}"/>
					<input type="hidden" name="com_name" value="{{$com_name}}"/>
					<input type="hidden" name="type" value="{{$type}}"/>
					<input type="hidden" name="status" value="{{$status}}"/>
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>


	</body>
	
	<script>
		$('#all').on('change',function(){
			var	tthis = $(this);
			var list = $('.main_list .list td input');
			if(tthis.is(':checked'))
			{
				list.attr('checked','checked');
				$('#pass').removeAttr('disabled');
				$('#unpass').removeAttr('disabled');
			}
			else
			{
				$('#pass').attr('disabled','disabled');
				$('#unpass').attr('disabled','disabled');
				list.removeAttr('checked');
			}
		});
		
		$('.main_list .list td input').on('change',function(){
			if($('.main_list .list td input:checked').length > 0)
			{
				$('#pass').removeAttr('disabled');
				$('#unpass').removeAttr('disabled');
			}
			else
			{
				$('#pass').attr('disabled','disabled');
				$('#unpass').attr('disabled','disabled');
				$('#all').removeAttr('checked');
			}
		});
		
		
		$('#pass,#unpass').click(function(){
			var list 	= $('.main_list .list td input:checked');
			var items 	= new Array();
			var act		= $(this).attr('id');  
			list.each(function(){  
				items.push($(this).attr('id'));
		   	});
		   	if(confirm('确定对这 '+items.length+' 项进行操作吗?'))
			{
				var url		= '{{action('Admin\CompanyController@companyHandle')}}';
				var data	= {'items':items,'act':act,'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
				},'json');
			}
		});
	</script>
</html>
