@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/company')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>企业名称</label>
					<div class="rl">
						<input type="text" name="com_name" value="{{$com->com_name}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>用户</label>
					<div class="rl">
						<input type="text" readonly="readonly" value="{{$com->name}}"/>
					</div>
				</div>

				
				<div class="row">
					<label>营业执照</label>

					<div class="rl" id="imglist">						
						@if($com->business_license != '')
						<div>
							<div class="img_box">
								<img id="b_l_l" src="{{asset($com->business_license)}}" width="100" height="140"/>
							</div>
						</div>
						@endif
						<li><input type="file" class="bn file" name="business_license" value=""/></li>
					</div>
				</div>
				
					
				<div class="row">
					<label>状态</label>
					<div class="rl">
						<input type="radio" class="cb" name="status" value="0"  @if($com->status == '0') checked="checked" @endif/><font>已认证</font>
						<input type="radio" class="cb" name="status" value="1"  @if($com->status == '1') checked="checked" @endif/><font>未认证</font>
					</div>
				</div>	

				<div class="row">
					<label>类型</label>
					<div class="rl">
						<input type="radio" class="cb" name="type" value="0"  @if($com->type == '0') checked="checked" @endif/><font>供应商</font>
						<input type="radio" class="cb" name="type" value="1"  @if($com->type == '1') checked="checked" @endif/><font>采购商</font>
					</div>
				</div>	

				<div class="row">
					<label>联系人</label>
					<div class="rl">
						<input type="text" name="linkman" value="{{$com->linkman}}"/>
						<b>*</b>	
					</div>
				</div>
				
				
				<div class="row">
					<label>联系电话</label>
					<div class="rl">
						<input type="text" name="telephone" value="{{$com->telephone}}"/>
						<b>*</b>	
					</div>
				</div>
				
								
				<div class="row">
					<label>区域</label>
					<div class="rl">
						<select name="province_id" id="province">
							<option value="">请选择...</option>
							@foreach($province as $p)
								<option @if($p->province_id == $com->province_id) selected="selected" @endif value="{{$p->province_id}}">{{$p->province}}</option>
							@endforeach
						</select>
						<select name="city_id" id="city" @if($com->city_id == '')style="display: none;"@endif>
							<option value="">请选择...</option>
							@foreach($cities as $c)
							<option @if($c->city_id == $com->city_id) selected="selected" @endif value="{{$c->city_id}}">{{$c->city}}</option>
							@endforeach
						</select>
						<select name="area_id" id="area" @if($com->area_id == '')style="display: none;"@endif>
							<option value="">请选择...</option>
							@foreach($areas as $a)
							<option @if($a->area_id == $com->area_id) selected="selected" @endif value="{{$a->area_id}}">{{$a->area}}</option>
							@endforeach
						</select>
					</div>
				</div>	
				
				<div class="row">
					<label>详细地址</label>
					<div class="rl">
						<textarea maxlength="100" class="address" name="address" >{{$com->address}}</textarea>
					</div>
				</div>
				
				<div class="row">
					<label>企业详情</label>
					<div class="rl">
						<textarea  name="desc" class="desc" >{{$com->desc}}</textarea>
					</div>
				</div>
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>

		<script>
		    $('#province').on('change',function(){
		    	var id   = $('#province').val();
		    	var url  = '{{action('Admin\ItemController@getCityByProvince')}}';
		    	var data = {'_token':'{{csrf_token()}}','province_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#city').show();
		    			$('#area').hide();
		    			$('#city option,#area option').remove();
		    			$('#city').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#city').append('<option value="'+res[i].city_id+'">'+res[i].city+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#city,#area').hide();	
		    			$('#city option,#area option').remove();
		    			$('#city,#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
		    
		    $('#city').on('change',function(){
		    	var id   = $('#city').val();
		    	var url  = '{{action('Admin\ItemController@getAreaByCity')}}';
		    	var data = {'_token':'{{csrf_token()}}','city_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#area').show();
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#area').append('<option value="'+res[i].area_id+'">'+res[i].area+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#area').hide();	
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
		    
		    $('.main_form .row .rl div .img_box img').click(function(){
		    	if($(this).css('width') == '100px')
		    	{
		    		$(this).css({'width':'600','height':'900'});	
		    	}
		    	else
		    	{
		    		$(this).css({'width':'100','height':'140'});
		    	}
				
		    });
		</script>
		
	</body>

</html>
