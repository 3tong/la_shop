@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/role')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>角色名称</label>
					<div class="rl">
						<input type="text" name="role_name" value="{{old('role_name')}}"/>
						<b>*</b>	
					</div>
				</div>
				<div class="row">
					<label>描述</label>
					<div class="rl">
						<textarea  class="desc" maxlength="200" name="desc">{{old('desc')}}</textarea>
					</div>
				</div>
	
				@foreach($acts as $v)
				<div class="row">
					<label><input type="checkbox" /><font>{{$v -> act_name}}</font></label>
					<div class="rl b_b_l">
						@foreach($v->list as $k => $m)
						<div class="ck"><input  @if( old('act_id') && in_array($m -> act_id, old('act_id') )) checked="checked" @endif  type="checkbox" name="act_id[]" value="{{$m -> act_id}}"/><font>{{$m -> act_name}}</font></div>
						@endforeach
					</div>
				</div>
				@endforeach

				
				<div class="row">
					<label><input type="checkbox" id="all"/><font>选择全部</font></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>

		<script>
			$('.main_form .row label input').on('change',function(){
				var tthis = $(this);
				var list  = tthis.parent('label').siblings('.b_b_l').find('input');
				if(tthis.is(':checked'))
				{
					list.attr('checked','checked');
				}
				else
				{
					list.removeAttr('checked');
				}

			});
			$('#all').on('change',function(){
				var tthis = $(this);
				var list  = $('input');
				if(tthis.is(':checked'))
				{
					list.attr('checked','checked');
				}
				else
				{
					list.removeAttr('checked');
				}	
			});
		</script>
		
	</body>

</html>

