@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/roleAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.role_add')}}</a>
			</h3>

			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>编号</th>
						<th>角色名</th>
						<th>描述</th>
						<th>操作</th>
					</tr>
					@forelse($data as $r)
					<tr>
						<td><i class="num">{{$r->role_id}}</i></td>
						<td><a href="{{url('admin/admin').'?role_id='.$r->role_id}}">{{$r->role_name}}</a></td>
						<td>{{$r->desc}}</td>
						<td><a href="" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/roleUpdate',[$r->role_id])}}">编辑</a>|<a class="fa fa-times del" aria-hidden="true"  role="{{$r->role_id}}">删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '')
			<div class="pager f_r">
				{!!$data->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	
		



	</body>
	<script>
		$('.del').click(function(){
			if(confirm('确定删除吗?'))
			{
				var tthis = $(this);
		    	var url		=	'{{action('Admin\RoleController@del')}}';
				var data	=   {'id':tthis.attr('role'),'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
					else
					{
						alert(res.content);
					}
				},'json');				
			}

		});
	</script>
</html>
