@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/role')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				
				<div class="row">
					<label>上级分类</label>
					<div class="rl">
						<select name="cat_id">
							<option value="0">顶级分类</option>
							@foreach($cates as $k=>$v)
								<option	@if(old('cat_id') == $v->cat_id )selected="selected" @endif value="{{$v->cat_id}}">{{str_repeat('&emsp;',$v->level)}}{{$v->cat_name}}</option>
							@endforeach
						</select>
						<b>*</b>						
					</div>
				</div>
				
				<div class="row">
					<label>品牌</label>
					<div class="rl">
						<select name="brand_id" id="brand">
							<option value="">请选择...</option>
							@foreach($brands as $b)
							<option value="{{$b->brand_id}}">{{$b->brand_name}}</option>
							@endforeach
						</select>
						<select name="model_id" id="model" style="display: none;">
							<option value="">选择型号</option>
						</select>					
					</div>
				</div>
				
				<div class="row">
					<label>区域</label>
					<div class="rl">
						<select name="province_id" id="province">
							<option value="">请选择...</option>
							@foreach($province as $p)
								<option value="{{$p->province_id}}">{{$p->province}}</option>
							@endforeach
						</select>
						<select name="city_id" id="city" style="display: none;">
							<option value="">请选择...</option>
						</select>
						<select name="area_id" id="area" style="display: none;">
							<option value="">请选择...</option>
						</select>
					</div>
				</div>	
				
				<div class="row">
					<label>选择文件</label>
					<div class="rl">
						<li><input type="file" class="bn excel" name="excel" <b>*</b></li>
					</div>
				</div>
				
				
				<div class="row">
					<label></label>
					<div class="rl">
						<li><a href="{{asset('/models/item.xlsx')}}">下载Excel模板</a></li>
					</div>
				</div>
				

								
				<div class="row">
					<label>使用说明</label>
					<div class="rl">
							<li>1. 碰到“是否热销”之类，填写数字 0 或者 1，1 代表“<font class="red"> 否</font>”，0 代表“<font class="green"> 是</font>”。</li>
							<li>2. 选择所上传商品的分类以及品牌和型号。</li>
							<li>3. 上传excel格式为csv，xls，xlsx且只能按照模板所提供的格式填写。</li>
							<li>4. 单个excel文件上传条数不建议大于1000。</li>
					</div>
				</div>
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>


		
	</body>
	<script>
			$('#brand').on('change',function(){
				var id 		 = $(this).val();
				var url		 = '{{action('Admin\BrandController@getModelByBrand')}}';
				var data	 = {'brand_id':id,'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.length == 0)
					{
						$('#model').hide();	
						$('#model option').remove();
						$('#model').append('<option value="">选择型号</option>');
					}
					else
					{
						$('#model').show();
						$('#model option').remove();
						$('#model').append('<option value="">选择型号</option>');
						for(var i=0;i<res.length;i++)
						{
							$('#model').append('<option value="'+res[i].model_id+'">'+res[i].model+'</option>');
						}	
					}
				},'json');
			});	
			
			$('#province').on('change',function(){
		    	var id   = $('#province').val();
		    	var url  = '{{action('Admin\ItemController@getCityByProvince')}}';
		    	var data = {'_token':'{{csrf_token()}}','province_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#city').show();
		    			$('#area').hide();
		    			$('#city option,#area option').remove();
		    			$('#city').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#city').append('<option value="'+res[i].city_id+'">'+res[i].city+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#city,#area').hide();	
		    			$('#city option,#area option').remove();
		    			$('#city,#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
		    
		    $('#city').on('change',function(){
		    	var id   = $('#city').val();
		    	var url  = '{{action('Admin\ItemController@getAreaByCity')}}';
		    	var data = {'_token':'{{csrf_token()}}','city_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#area').show();
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#area').append('<option value="'+res[i].area_id+'">'+res[i].area+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#area').hide();	
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
			    
	</script>
</html>

