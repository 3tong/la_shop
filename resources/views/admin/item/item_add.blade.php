@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/item')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>商品名称</label>
					<div class="rl">
						<input type="text" name="item_name" value="{{old('item_name')}}"/>
						<b>*</b>	
					</div>
				</div>
				<div class="row">
					<label>商品货号</label>
					<div class="rl">
						<input type="text" name="item_sn" value="{{old('item_sn')}}" />
					</div>
				</div>
				<div class="row">
					<label>所属分类</label>
					<div class="rl">
						<select name="cat_id">
							<option value="0">顶级分类</option>
							@foreach($cates as $c)
							<option value="{{$c->cat_id}}">{{str_repeat('&emsp;',$c->level)}}{{$c->cat_name}}</option>
							@endforeach
						</select>
						<b>*</b>						
					</div>
				</div>
				<div class="row">
					<label>品牌</label>
					<div class="rl">
						<select name="brand_id" id="brand">
							<option value="">请选择...</option>
							@foreach($brands as $b)
							<option value="{{$b->brand_id}}">{{$b->brand_name}}</option>
							@endforeach
						</select>
						<select name="model_id" id="model" style="display: none;">
							<option value="">选择型号</option>
						</select>					
					</div>
				</div>

				<div class="row">
					<label>企业</label>
					<div class="rl">
						<select name="com_id">
							<option value="">请选择...</option>
							@foreach($companies as $c)
							<option value="{{$c->com_id}}">{{$c->com_name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<label>区域</label>
					<div class="rl">
						<select name="province_id" id="province">
							<option value="">请选择...</option>
							@foreach($province as $p)
								<option value="{{$p->province_id}}">{{$p->province}}</option>
							@endforeach
						</select>
						<select name="city_id" id="city" style="display: none;">
							<option value="">请选择...</option>
						</select>
						<select name="area_id" id="area" style="display: none;">
							<option value="">请选择...</option>
						</select>
					</div>
				</div>			
				<div class="row">
					<label>库存</label>
					<div class="rl">
						<input type="text" name="number" value="99"/>
						<b>*</b>			
					</div>
				</div>		
				<div class="row">
					<label>市场价</label>
					<div class="rl">
						<input type="text" class="lt price" value="{{old('market_price')}}" name="market_price"/><font class="price">元</font> 
						<b>*</b>
					</div>
				</div>		
				<div class="row">
					<label>价格</label>
					<div class="rl">
						<input type="text" class="price" value="{{old('price')}}" name="price"/><font class="price">元</font>
						<b>*</b>						
					</div>
				</div>		
				<div class="row">
					<label>状态</label>
					<div class="rl">
						<input type="radio" class="cb" name="status" value="0"  checked="checked"/><font>通过</font>
						<input type="radio" class="cb" name="status" value="1" /><font>未审核</font>
						<input type="radio" class="cb" name="status" value="2" /><font>不通过</font>
					</div>
				</div>		
				<div class="row">
					<label>上架</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_sale" value="0" checked="checked" /><font>是</font>
						<input type="radio" class="cb" name="is_sale" value="1"/><font>否</font>
					</div>
				</div>		
				<div class="row">
					<label>热销</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_hot" value="0" /><font>是</font>
						<input type="radio" class="cb" name="is_hot" value="1" checked="checked" /><font>否</font>
					</div>
				</div>		
				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" value="0" name="sort_order"/>	
					</div>
				</div>
				<div class="row">
					<label>销量</label>
					<div class="rl">
						<input type="text" value="@if(old('sales_number')){{old('sales_number')}}@else 0 @endif" name="sales_number"/>	
					</div>
				</div>
				<div class="row">
					<label>点击次数</label>
					<div class="rl">
						<input type="text" value="@if(old('click')){{old('click')}}@else 0 @endif" name="click"/>	
					</div>
				</div>
				<div class="row">
					<label>关键字</label>
					<div class="rl">
						<input type="text" class="kw" value="{{old('keyword')}}" name="keyword"/>	
					</div>
				</div>
				<div class="row">
					<label>商品图片</label>
					<div class="rl" id="imglist">
						<li><input type="file" class="bn file" name="img[]" value=""/>&emsp;<font>图片描述 :</font><input type="text" name="img_desc[]" />&emsp;<a class="sub" id="addimg"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;增加</a></li>
					</div>
				</div>
				<div class="row">
					<label>商品属性</label>
					<div class="rl" id="attrlist">
						<li><font>属性名 :</font><input type="text" name="attr_name[]" value=""/>&emsp;<font>属性值 :</font><input type="text" name="attr_value[]" />&emsp;<a class="sub" id="addattr"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;增加</a></li>
					</div>
				</div>								
				<div class="row">
					<label>商品详情</label>
					<div class="rl">
						<script id="container" name="desc" type="text/plain">{!!old('desc')!!}</script>	
					</div>
				</div>	
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>					
					</div>
				</div>	
			</form>			
		</div>

		<script type="text/javascript">
		    var ue = UE.getEditor('container');
			//实例化编辑器
		    ue.ready(function() {
		       ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
		    });

		    $('#province').on('change',function(){
		    	var id   = $('#province').val();
		    	var url  = '{{action('Admin\ItemController@getCityByProvince')}}';
		    	var data = {'_token':'{{csrf_token()}}','province_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#city').show();
		    			$('#area').hide();
		    			$('#city option,#area option').remove();
		    			$('#city').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#city').append('<option value="'+res[i].city_id+'">'+res[i].city+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#city,#area').hide();	
		    			$('#city option,#area option').remove();
		    			$('#city,#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
		    
		    $('#city').on('change',function(){
		    	var id   = $('#city').val();
		    	var url  = '{{action('Admin\ItemController@getAreaByCity')}}';
		    	var data = {'_token':'{{csrf_token()}}','city_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#area').show();
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#area').append('<option value="'+res[i].area_id+'">'+res[i].area+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#area').hide();	
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
		    
			$('#brand').on('change',function(){
				var id 		 = $(this).val();
				var url		 = '{{action('Admin\BrandController@getModelByBrand')}}';
				var data	 = {'brand_id':id,'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.length == 0)
					{
						$('#model').hide();	
						$('#model option').remove();
						$('#model').append('<option value="">选择型号</option>');
					}
					else
					{
						$('#model').show();
						$('#model option').remove();
						$('#model').append('<option value="">选择型号</option>');
						for(var i=0;i<res.length;i++)
						{
							$('#model').append('<option value="'+res[i].model_id+'">'+res[i].model+'</option>');
						}	
					}
				},'json');
			});
		    
		    $('#addimg').click(function(){
		    	if($('#imglist li').length >= 5)
		    	{
					alert('每个商品最多只能添加5张图片');		    		
		    	}
		    	else
		    	{
					$('#imglist').append('<li><input type="file" class="bn file" name="img[]" value=""/>&emsp;<font>图片描述 :</font><input type="text" name="img_desc[]"/>&emsp;<a class="sub red" id="delimg"><i class="fa fa-times" aria-hidden="true"></i></i>&nbsp;删除</a></li>');		    		
		    	}	
		    });
		    
		    $('#delimg').live('click',function(){
		    	$(this).parent('li').remove();
		    })
		    
	    	$('#addattr').click(function(){
		    	if($('#attrlist li').length >= 5)
		    	{
					alert('每个商品最多只能添加5种属性');		    		
		    	}
		    	else
		    	{
					$('#attrlist').append('<li><font>属性名 :</font><input type="text" name="attr_name[]" value=""/>&emsp;<font>属性值 :</font><input type="text" name="attr_value[]" />&emsp;<a class="sub red" id="delattr"><i class="fa fa-times" aria-hidden="true"></i></i>&nbsp;删除</a></li>');		    		
		    	}	
		    });
		    
		    $('#delattr').live('click',function(){
		    	$(this).parent('li').remove();
		    })
		    
		    
		</script>
		
	</body>

</html>
