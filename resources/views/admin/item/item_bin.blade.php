@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/item')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action="" method="get" class="search">
				<p id="warning" style="display: none">Tip:<b id="#info"></b></p>
				<li><b>编号&nbsp;:&nbsp;</b><input class="s" name="item_id"  value="{{$item_id}}"/></li>
				<li><b>货号&nbsp;:&nbsp;</b><input type="text" name="item_sn"  value="{{$item_sn}}"/></li>
				<li>
					<b>分类&nbsp;:&nbsp;</b>
					<select name="cat_id">
						<option value="">全部</option>
						@foreach($cates as $k=>$v)
							<option value="{{$v->cat_id}}" @if($v->cat_id == $cat_id)selected="selected"@endif>{{str_repeat('&emsp;',$v->level)}}{{$v->cat_name}}</option>
						@endforeach
					</select>
				</li>
				<li>
					<b>企业&nbsp;:&nbsp;</b>
					<select name="com_id">
						<option value="">全部</option>
						@foreach($companies as $k=>$v)
							<option value="{{$v->com_id}}" @if($v->com_id == $com_id)selected="selected"@endif>{{$v->com_name}}</option>
						@endforeach
					</select>
				</li>
				<li id="brand">
					<b>品牌&nbsp;:&nbsp;</b>
					<select name="brand_id">
						<option value="">全部</option>
						@foreach($brands as $k=>$v)
							<option value="{{$v->brand_id}}" @if($v->brand_id == $brand_id)selected="selected"@endif>{{$v->brand_name}}</option>
						@endforeach
					</select>
				</li>
				<li id="model" style="@if(!$models)display:none;@endif">
					<b>型号&nbsp;:&nbsp;</b>
					<select name="model_id" @if($v->model_id == $model_id)selected="selected"@endif>
						<option value="">全部</option>
						@if( $models !== '')
							@foreach($models as $v)
								<option value="{{$v->model_id}}" @if($v->model_id == $model_id)selected="selected"@endif>{{$v->model}}</option>
							@endforeach
						@endif
					</select>
				</li>
				<li>
					<b>状态&nbsp;:&nbsp;</b>
					<select name="status">
						<option value="">全部</option>
						<option value="0" @if($status == '0')selected="selected"@endif>已通过</option>
						<option value="1" @if($status == '1')selected="selected"@endif>未审核</option>
						<option value="2" @if($status == '2')selected="selected"@endif>未通过</option>
					</select>
				</li>
				<li>
					<b>上架&nbsp;:&nbsp;</b>
					<select class="s" name="is_sale">
						<option value="">全部</option>
						<option value="0" @if($is_sale == '0')selected="selected"@endif>上架</option>
						<option value="1" @if($is_sale == '1')selected="selected"@endif>下架</option>
					</select>
				</li>
				<li>
					<b>热销&nbsp;:&nbsp;</b>
					<select class="s" name="is_hot">
						<option value="">全部</option>
						<option value="0" @if($is_hot == '0')selected="selected"@endif>是</option>
						<option value="1" @if($is_hot == '1')selected="selected"@endif>否</option>
					</select>
				</li>
				<li><b>关键字&nbsp;:&nbsp;</b><input type="text" value="{{$keyword}}"  placeholder="品名/品牌/型号" name="keyword"/></li>				
				<li><input type="submit" value="查询" class="submit"/></li>
			</form>
			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>选择</th>
						<th>编号</th>
						<th>商品名称</th>
						<th>货号</th>
						<th>分类</th>
						<th>品牌</th>
						<th>型号</th>
						<th>库存</th>
						<th>吨价</th>
						<th>状态</th>
						<th>上架</th>
						<th>排序</th>
						<th>操作</th>
					</tr>
					@forelse($data as $i)
					<tr>
						<td><input type="checkbox" id="{{$i->item_id}}"/></td>
						<td><i class="num">{{$i->item_id}}</i></td>
						<td><a href="">{{$i->item_name}}</a></td>
						<td><i class="num">{{$i->item_sn}}</i></td>
						<td>{{$i->cat_name}}</td>
						<td>{{$i->brand_name}}</td>
						<td>{{$i->model}}</td>
						<td><font class="green">{{$i->number}}</font></td>
						<td><font class="price">{{$i->price}}</font></td>
						<td>
							@if($i->status == '0')<font class="green">已通过</font>
							@elseif($i->status == '1')<font class="red">未审核</font>
							@elseif($i->status == '2')<font class="red">未通过</font>
							@endif
						</td>
						<td>
							@if($i->is_sale == '0')<font class="green">上架</font>
							@elseif($i->is_sale == '1')<font class="red">下架</font>
							@endif
						</td>
						<td>
							<i class="num">{{$i->sort_order}}</i>
						</td>
						<td><a href="{{url('admin/itemRecover',[$i->item_id])}}';}" class="fa fa-refresh" aria-hidden="true">恢复</a>|<a class="fa fa-times" aria-hidden="true" onclick="if(confirm('确定将该商品彻底删除吗？这步操作将无法恢复。')){window.location.href='{{url('admin/itemDelTrue',[$i->item_id])}}';}">彻底删除</a></td>
					</tr>
				 	@empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse			
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '' || $data->count()>0)
			<div class="handle_box">
				<input type="checkbox" id="all"/><font>全选</font>
				<button disabled="disabled" id="del_true"><i class="fa fa-trash" aria-hidden="true"></i>彻底删除</button>
				<button disabled="disabled" id="recover"><i class="fa fa-refresh" aria-hidden="true"></i>恢复</button>
			</div>
			@endif
			@if($data != '')
			<div class="pager f_r">
				{!!$data->appends(['item_id'	=>	$item_id,
					'item_sn'	=>	$item_sn,
					'cat_id'	=>	$cat_id	,
					'com_id'	=>	$com_id	,
				    'brand_id'	=>	$brand_id,
				    'model_id'	=>	$model_id,
					'status'	=>	$status	,
				    'is_sale'	=>	$is_sale,
					'is_hot'	=>	$is_hot	,
				    'keyword'	=>	$keyword
				])->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="hidden" name="item_id" value="{{$item_id}}" />
					<input type="hidden" name="item_sn" value="{{$item_sn}}" />
					<input type="hidden" name="cat_id" value="{{$cat_id}}" />
					<input type="hidden" name="com_id" value="{{$com_id}}" />
					<input type="hidden" name="brand_id" value="{{$brand_id}}" />
					<input type="hidden" name="model_id" value="{{$model_id}}" />
					<input type="hidden" name="status" value="{{$status}}" />
					<input type="hidden" name="is_sale" value="{{$is_sale}}" />
					<input type="hidden" name="is_hot" value="{{$is_hot}}" />
					<input type="hidden" name="keyword" value="{{$keyword}}" />		
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>			
			@endif	
		</div>
		
		


	</body>
	<script>
		$('#brand select').on('change',function(){
			var brand_id = $(this).val();
			var url		 = '{{action('Admin\BrandController@getModelByBrand')}}';
			var data	 = {'brand_id':brand_id,'_token':'{{csrf_token()}}'};
			$.post(url,data,function(res){
				if(res.length == 0)
				{
					$('#model').hide();	
					$('#model select option').remove();
					$('#model select').append('<option value="">全部</option>');
				}
				else
				{
					$('#model').show();
					$('#model select option').remove();
					$('#model select').append('<option value="">全部</option>');
					for(var i=0;i<res.length;i++)
					{
						$('#model select').append('<option value="'+res[i].model_id+'">'+res[i].model+'</option>');
					}	
				}
			},'json');
		});
		
		$('#all').on('change',function(){
			var	tthis = $(this);
			var list = $('.main_list .list td input');
			if(tthis.is(':checked'))
			{
				list.attr('checked','checked');
				$('#del_true').removeAttr('disabled');
				$('#recover').removeAttr('disabled');
			}
			else
			{
				$('#del_true').attr('disabled','disabled');
				$('#recover').attr('disabled','disabled');
				list.removeAttr('checked');
			}
		});
		
		$('.main_list .list td input').on('change',function(){
			if($('.main_list .list td input:checked').length > 0)
			{
				$('#del_true').removeAttr('disabled');
				$('#recover').removeAttr('disabled');
			}
			else
			{
				$('#del_true').attr('disabled','disabled');
				$('#recover').attr('disabled','disabled');
				$('#all').removeAttr('checked');
			}
		});
		
		
		$('#del_true,#recover').click(function(){
			var list 	= $('.main_list .list td input:checked');
			var items 	= new Array();
			var act		= $(this).attr('id');  
			list.each(function(){  
				items.push($(this).attr('id'));
		   	});
		   	if(confirm('确定对这 '+items.length+' 项进行操作吗?'))
			{
				var url		= '{{action('Admin\ItemController@itemHandle')}}';
				var data	= {'items':items,'act':act,'_token':'{{csrf_token()}}'};
				console.log(data);
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
				},'json');
			}
		});
		

		
	</script>
</html>
