@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
	
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/item')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>商品名称</label>
					<div class="rl">
						<input type="text" name="item_name" value="{{$item->item_name}}"/>
						<b>*</b>	
					</div>
				</div>
				<div class="row">
					<label>商品货号</label>
					<div class="rl">
						<input type="text" name="item_sn" value="{{$item->item_sn}}" />
					</div>
				</div>
				<div class="row">
					<label>所属分类</label>
					<div class="rl">
						<select name="cat_id">
							<option value="0">顶级分类</option>
							@foreach($cates as $c)
							<option @if($c->cat_id == $item->cat_id) selected="selected" @endif value="{{$c->cat_id}}">{{str_repeat('&emsp;',$c->level)}}{{$c->cat_name}}</option>
							@endforeach
						</select>
						<b>*</b>						
					</div>
				</div>
				<div class="row">
					<label>品牌</label>
					<div class="rl">
						<select name="brand_id" id="brand">
							<option value="">请选择...</option>
							@foreach($brands as $b)
							<option @if($b->brand_id == $item->brand_id) selected="selected" @endif value="{{$b->brand_id}}">{{$b->brand_name}}</option>
							@endforeach
						</select>
						<select name="model_id" id="model" @if($item->model_id == '')style="display: none;"@endif>
							<option value="">选择型号</option>
							@foreach($models as $m)
							<option @if($m->model_id == $item->model_id) selected="selected" @endif value="{{$m->model_id}}">{{$m->model}}</option>
							@endforeach
						</select>					
					</div>
				</div>

				<div class="row">
					<label>企业</label>
					<div class="rl">
						<select name="com_id">
							<option value="">请选择...</option>
							@foreach($companies as $c)
							<option @if($c->com_id == $item->com_id) selected="selected" @endif value="{{$c->com_id}}">{{$c->com_name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<label>区域</label>
					<div class="rl">
						<select name="province_id" id="province">
							<option value="">请选择...</option>
							@foreach($province as $p)
								<option @if($p->province_id == $item->province_id) selected="selected" @endif value="{{$p->province_id}}">{{$p->province}}</option>
							@endforeach
						</select>
						<select name="city_id" id="city" @if($item->city_id == '')style="display: none;"@endif>
							<option value="">请选择...</option>
							@foreach($cities as $c)
							<option @if($c->city_id == $item->city_id) selected="selected" @endif value="{{$c->city_id}}">{{$c->city}}</option>
							@endforeach
						</select>
						<select name="area_id" id="area" @if($item->area_id == '')style="display: none;"@endif>
							<option value="">请选择...</option>
							@foreach($areas as $a)
							<option @if($a->area_id == $item->area_id) selected="selected" @endif value="{{$a->area_id}}">{{$a->area}}</option>
							@endforeach
						</select>
					</div>
				</div>			
				<div class="row">
					<label>库存</label>
					<div class="rl">
						<input type="text" name="number" value="{{$item->number}}"/>
						<b>*</b>			
					</div>
				</div>		
				<div class="row">
					<label>市场价</label>
					<div class="rl">
						<input type="text" class="lt price" value="{{$item->market_price}}" name="market_price"/><font class="price">元</font> 
						<b>*</b>
					</div>
				</div>		
				<div class="row">
					<label>价格</label>
					<div class="rl">
						<input type="text" class="price" value="{{$item->price}}" name="price"/><font class="price">元</font>
						<b>*</b>						
					</div>
				</div>		
				<div class="row">
					<label>状态</label>
					<div class="rl">
						<input type="radio" class="cb" name="status" value="0" @if($item->status == '0') checked="checked" @endif/><font>通过</font>
						<input type="radio" class="cb" name="status" value="1" @if($item->status == '1') checked="checked" @endif/><font>未审核</font>
						<input type="radio" class="cb" name="status" value="2" @if($item->status == '2') checked="checked" @endif/><font>不通过</font>
					</div>
				</div>		
				<div class="row">
					<label>上架</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_sale" value="0" @if($item->is_sale == '0') checked="checked" @endif/><font>是</font>
						<input type="radio" class="cb" name="is_sale" value="1" @if($item->is_sale == '1') checked="checked" @endif/><font>否</font>
					</div>
				</div>		
				<div class="row">
					<label>热销</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_hot" value="0" @if($item->is_hot == '0') checked="checked" @endif/><font>是</font>
						<input type="radio" class="cb" name="is_hot" value="1" @if($item->is_hot == '1') checked="checked" @endif/><font>否</font>
					</div>
				</div>		
				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" value="{{$item->sort_order}}" name="sort_order" />	
					</div>
				</div>
				<div class="row">
					<label>销量</label>
					<div class="rl">
						<input type="text" value="{{$item->sales_number}}" name="sales_number"/>	
					</div>
				</div>
				<div class="row">
					<label>点击次数</label>
					<div class="rl">
						<input type="text" value="{{$item->click}}" name="click"/>	
					</div>
				</div>
				<div class="row">
					<label>关键字</label>
					<div class="rl">
						<input type="text" class="kw" value="{{$item->keyword}}" name="keyword"/>	
						<b>如改动商品信息建议删除原关键字，系统将重新生成。</b>
					</div>
				</div>
				<div class="row">
					<label>商品图片</label>
					<div class="rl" id="imglist">
						<div>
							@foreach( $imgs as $i)
							<div class="img_box" img_id="{{$i->img_id}}" >
								<img src="{{asset($i->tiny_img)}}" alt="{{$i->img_desc}}" title="{{$i->img_desc}}"/>
								<input type="text" placeholder="图片描述" value="{{$i->img_desc}}"/>
								<a class="sub red"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;删除</a>
							</div>
							@endforeach
						</div>
						<li><input type="file" class="bn file" name="img[]" value=""/>&emsp;<font>图片描述 :</font><input type="text" name="img_desc[]" />&emsp;<a class="sub" id="addimg"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;增加</a></li>
					</div>
				</div>
				<div class="row">
					<label>商品属性</label>
					<div class="rl" id="attrlist">
						@foreach( $attrs as $a)
							<li attr_id="{{$a->attr_id}}"><font>属性名 :</font><input class="attr_update" act="attr_name" type="text" value="{{$a->attr_name}}"/>&emsp;<font>属性值 :</font><input class="attr_update" type="text" act="attr_value" value="{{$a->attr_value}}"/>&emsp;<a class="sub red attr_del"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;删除</a></li>
						@endforeach
						<li><font>属性名 :</font><input type="text" name="attr_name[]" value=""/>&emsp;<font>属性值 :</font><input type="text" name="attr_value[]" />&emsp;<a class="sub" id="addattr"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;增加</a></li>
					</div>
				</div>								
				<div class="row">
					<label>商品详情</label>
					<div class="rl">
						<script id="container" name="desc" type="text/plain">{!!$item->desc!!}</script>	
					</div>
				</div>	
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />	
						<b>带‘*’的为必填项。</b>								
					</div>
				</div>	
			</form>			
		</div>

		<script type="text/javascript">
		    var ue = UE.getEditor('container');
			//实例化编辑器
		    ue.ready(function() {
		       ue.execCommand('serverparam', '_token', '{{ csrf_token() }}'); // 设置 CSRF token.
		    });

		    $('#province').on('change',function(){
		    	var id   = $('#province').val();
		    	var url  = '{{action('Admin\ItemController@getCityByProvince')}}';
		    	var data = {'_token':'{{csrf_token()}}','province_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#city').show();
		    			$('#area').hide();
		    			$('#city option,#area option').remove();
		    			$('#city').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#city').append('<option value="'+res[i].city_id+'">'+res[i].city+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#city,#area').hide();	
		    			$('#city option,#area option').remove();
		    			$('#city,#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
		    
		    $('#city').on('change',function(){
		    	var id   = $('#city').val();
		    	var url  = '{{action('Admin\ItemController@getAreaByCity')}}';
		    	var data = {'_token':'{{csrf_token()}}','city_id':id}
		    	$.post(url,data,function(res){
		    		if(res.length != 0)
		    		{
		    			$('#area').show();
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    			for(var i = 0; i< res.length; i++)
		    			{
		    				$('#area').append('<option value="'+res[i].area_id+'">'+res[i].area+'</option>');		
		    			}
		    		}
		    		else
		    		{
		    			$('#area').hide();	
		    			$('#area option').remove();
		    			$('#area').append('<option value="">请选择...</option>');
		    		}
		    	},'json'); 
		    });
		    
			$('#brand').on('change',function(){
				var id 		 = $(this).val();
				var url		 = '{{action('Admin\BrandController@getModelByBrand')}}';
				var data	 = {'brand_id':id,'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.length == 0)
					{
						$('#model').hide();	
						$('#model option').remove();
						$('#model').append('<option value="">选择型号</option>');
					}
					else
					{
						$('#model').show();
						$('#model option').remove();
						$('#model').append('<option value="">选择型号</option>');
						for(var i=0;i<res.length;i++)
						{
							$('#model').append('<option value="'+res[i].model_id+'">'+res[i].model+'</option>');
						}	
					}
				},'json');
			});
		    
		    $('#addimg').click(function(){
		    	if($('#imglist li').length >= 5)
		    	{
					alert('每个商品最多只能添加5张图片');		    		
		    	}
		    	else
		    	{
					$('#imglist').append('<li><input type="file" class="bn file" name="img[]" value=""/>&emsp;<font>图片描述 :</font><input type="text" name="img_desc[]"/>&emsp;<a class="sub red" id="delimg"><i class="fa fa-times" aria-hidden="true"></i></i>&nbsp;删除</a></li>');		    		
		    	}	
		    });
		    
		    $('#delimg').live('click',function(){
		    	$(this).parent('li').remove();
		    });
		    
	    	$('#addattr').click(function(){
		    	if($('#attrlist li').length >= 5)
		    	{
					alert('每个商品最多只能添加5种属性');		    		
		    	}
		    	else
		    	{
					$('#attrlist').append('<li><font>属性名 :</font><input type="text" name="attr_name[]" value=""/>&emsp;<font>属性值 :</font><input type="text" name="attr_value[]" />&emsp;<a class="sub red" id="delattr"><i class="fa fa-times" aria-hidden="true"></i></i>&nbsp;删除</a></li>');		    		
		    	}	
		    });
		    
		    $('#delattr').live('click',function(){
		    	$(this).parent('li').remove();
		    });
		    
		    $('.main_form .row .rl div .img_box a').click(function(){
		    	if(confirm('确定删除吗？'))
		    	{
			    	var tthis	=	$(this);
			    	var img_box	=	tthis.parent('div');
			    	var id    	=	img_box.attr('img_id');
			    	var url		=	'{{action('Admin\ItemController@imgDel')}}';
					var data	= {'img_id':id,'_token':'{{csrf_token()}}'};
			    	$.post(url,data,function(res){
			    		if(res.error == 0)
			    		{
			    			img_box.remove();
			    		}
			    	},'json');
		    	}
		    });
		    
		    $('.main_form .row .rl div .img_box input').blur(function(){
		    	var tthis	=	$(this);
		    	var id    	=	tthis.parent('div').attr('img_id');
		    	var url		=	'{{action('Admin\ItemController@imgDescUpdate')}}';
				var data	= {'img_id':id,'desc':tthis.val(),'_token':'{{csrf_token()}}'};
		    	$.post(url,data,function(res){
		    		if(res.error != 0)
		    		{
		    			alert('服务器内部错误！');
		    		}
		    	},'json');   	
		    });
		    
		    $('.attr_del').click(function(){
	    		if(confirm('确定删除吗？'))
		    	{
			    	var tthis		= $(this);
			    	var attr_box	= tthis.parent('li');
			    	var	id			= attr_box.attr('attr_id');
			    	var url		=	'{{action('Admin\ItemController@attrDel')}}';
					var data	=  	{'attr_id':id,'_token':'{{csrf_token()}}'};
			    	$.post(url,data,function(res){
			    		if(res.error == 0)
			    		{
			    			attr_box.remove();
			    		}
			    	},'json');
		    	}
		    });
		    
		    $('.attr_update').blur(function(){
		    	var tthis		= $(this);
		    	var attr_box	= tthis.parent('li');
		    	var	id			= attr_box.attr('attr_id');
		    	var url		=	'{{action('Admin\ItemController@attrUpdate')}}';
				var data	= 	{'attr_id':id,'value':tthis.val(),'act':tthis.attr('act'),'_token':'{{csrf_token()}}'};
		    	$.post(url,data,function(res){
		    		if(res.error != 0)
		    		{
		    			alert('服务器内部错误！');
		    		}
		    	},'json');
		    });
		    
		    
		    
		</script>
		
	</body>

</html>
