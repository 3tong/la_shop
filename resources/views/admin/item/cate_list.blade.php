@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/itemCateAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.cate_add')}}</a>
			</h3>

			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>编号</th>
						<th>分类名称</th>
						<th>单位</th>
						<th>分类描述</th>
						<th>是否显示</th>
						<th>排序</th>
						<th>操作</th>
					</tr>
					@forelse($data as $a)
					<tr>
						<td><i class="num">{{$a->cat_id}}</i></td>
						<td style="text-align: left; padding-left:10px;">{{str_repeat('&emsp;&emsp;',$a->level-1)}}<a href="{{url('admin/item/').'?item_id=&item_sn=&cat_id='.$a->cat_id.'&com_id=&brand_id=&model_id=&status=&is_sale=&is_hot=&keyword='}}">{{$a->cat_name}}</a></td>
						<td><font class="green">{{$a->unit}}</font></td>
						<td>{{$a->desc}}</td>
						<td>@if($a->is_show == '0')<font class="green">是</font>@else<font class="red">否</font>@endif</td>						
						<td>{{$a->sort_order}}</td>
						<td><a href="" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/itemCateUpdate',[$a->cat_id])}}">编辑</a>|<a class="fa fa-times del" aria-hidden="true"  cat_id="{{$a->cat_id}}">删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		

		



	</body>
	<script>
		$('.del').click(function(){
			if(confirm('确定删除吗?'))
			{
				var tthis = $(this);
		    	var url		=	'{{action('Admin\ItemController@cateDel')}}';
				var data	=   {'id':tthis.attr('cat_id'),'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
					else
					{
						alert(res.content);
					}
				},'json');				
			}

		});
	</script>

</html>
