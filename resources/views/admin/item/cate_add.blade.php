@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/itemCate')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				
				<div class="row">
					<label>上级分类</label>
					<div class="rl">
						<select name="parent_id">
							<option value="0">顶级分类</option>
							@foreach($cates as $k=>$v)
								<option value="{{$v->cat_id}}">{{str_repeat('&emsp;',$v->level)}}{{$v->cat_name}}</option>
							@endforeach
						</select>
						<b>*</b>						
					</div>
				</div>
				
				
				<div class="row">
					<label>分类名称</label>
					<div class="rl">
						<input type="text" name="cat_name" value="{{old('cat_name')}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>商品单位</label>
					<div class="rl">
						<input type="text" name="unit" placeholder="例如：包，桶，袋" value="{{old('unit')}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>显示</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_show" checked="checked" value="0"><font>是</font>
						<input type="radio" class="cb" name="is_show" value="1"><font>否</font>
					</div>
				</div>
				
				
				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" name="sort_order" 	value="@if(!old('sort_order'))0 @else{{old('sort_order')}}@endif"/>
					</div>
				</div>

				<div class="row">
					<label>分类描述</label>
					<div class="rl">
						<textarea name="desc" class="desc"></textarea>
					</div>
				</div>
				
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>

		<script>

		</script>
		
	</body>

</html>

