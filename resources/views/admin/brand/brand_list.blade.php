@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/brandAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.brand_add')}}</a>
			</h3>
			<form action="" method="get" class="search">
				<p id="warning" style="display: none">Tip:<b id="#info"></b></p>
				<li><b>编号&nbsp;:&nbsp;</b><input type="text"  name="brand_id"  	value="{{$brand_id}}"/></li>
				<li><b>品牌名称&nbsp;:&nbsp;</b><input type="text" name="brand_name"  value="{{$brand_name}}"/></li>
				<li><b>推荐&nbsp;:&nbsp;</b>
					<select name="is_hot">
						<option value=""  @if($is_hot == '') selected="selected" @endif>全部</option>
						<option value="0" @if($is_hot == '0') selected="selected" @endif>是</option>
						<option value="1" @if($is_hot == '1') selected="selected" @endif>否</option>
					</select>
				</li>
				<li><input type="submit" value="查询" class="submit"/></li>
			</form>
			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>选择</th>
						<th>编号</th>
						<th>品牌名称</th>
						<th>地区</th>
						<th>详细地址</th>
						<th>官网</th>
						<th>推荐</th>
						<th>排序</th>
						<th>操作</th>
					</tr>
					@forelse($data as $b)
					<tr>
						<td><input type="checkbox" id="{{$b->brand_id}}"/></td>
						<td><i class="num">{{$b->brand_id}}</i></td>
						<td><a href="">{{$b->brand_name}}</a></td>
						<td>{{$b->area}}</td>
						<td>{{$b->address}}</td>
						<td><a target="_blank" href="{{$b->website}}">打开网站</a></td>
						<td>@if($b->is_hot == '0')<font class="green">是</font>@elseif($b->is_hot == '1')<font class="red">否</font>@endif</td>
						<td><i class="num">{{$b->sort_order}}</i></td>
						<td><a href="" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/brandUpdate',[$b->brand_id])}}">编辑</a>|<a class="fa fa-times del" aria-hidden="true"  brand="{{$b->brand_id}}">删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '' || $data->count()>0)
			<div class="handle_box">
				<input type="checkbox" id="all"/><font>全选</font>
				<button disabled="disabled" id="hot"><i class="fa fa-check" aria-hidden="true"></i>设置推荐</button>
				<button disabled="disabled" id="unhot"><i class="fa fa-times" aria-hidden="true"></i>取消推荐</button>
			</div>
			@endif
			@if($data != '')
			<div class="pager f_r">
				{!!$data->appends(['brand_id' => $brand_id,'brand_name'=> $brand_name,'is_hot'=>$is_hot])->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="hidden" name="brand_id" value="{{$brand_id}}"/>
					<input type="hidden" name="brand_name" value="{{$brand_name}}"/>
					<input type="hidden" name="is_hot" value="{{$is_hot}}"/>
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	




	</body>
	
	<script>
		$('#all').on('change',function(){
			var	tthis = $(this);
			var list = $('.main_list .list td input');
			if(tthis.is(':checked'))
			{
				list.attr('checked','checked');
				$('#hot').removeAttr('disabled');
				$('#unhot').removeAttr('disabled');
			}
			else
			{
				$('#hot').attr('disabled','disabled');
				$('#unhot').attr('disabled','disabled');
				list.removeAttr('checked');
			}
		});
		
		$('.main_list .list td input').on('change',function(){
			if($('.main_list .list td input:checked').length > 0)
			{
				$('#hot').removeAttr('disabled');
				$('#unhot').removeAttr('disabled');
			}
			else
			{
				$('#hot').attr('disabled','disabled');
				$('#unhot').attr('disabled','disabled');
				$('#all').removeAttr('checked');
			}
		});
		
		
		$('#hot,#unhot').click(function(){
			var list 	= $('.main_list .list td input:checked');
			var items 	= new Array();
			var act		= $(this).attr('id');  
			list.each(function(){  
				items.push($(this).attr('id'));
		   	});
		   	if(confirm('确定对这 '+items.length+' 项进行操作吗?'))
			{
				var url		= '{{action('Admin\BrandController@brandHandle')}}';
				var data	= {'items':items,'act':act,'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
				},'json');
			}
		});
		
		$('.del').click(function(){
			if(confirm('确定删除吗?'))
			{
				var tthis = $(this);
		    	var url		=	'{{action('Admin\BrandController@del')}}';
				var data	=   {'id':tthis.attr('brand'),'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
					else
					{
						alert(res.content);
					}
				},'json');				
			}

		});

	</script>
</html>
