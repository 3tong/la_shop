@include('admin.header')


		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/brand')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>品牌名称</label>
					<div class="rl">
						<input type="text" name="brand_name" value="{{$brand->brand_name}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>Logo</label>
					<div class="rl" id="imglist">
						@if($brand->brand_logo != '')
						<div>
							<div class="img_box">
								<img id="b_l_l" src="{{asset($brand->brand_logo)}}" style="width:120px; height:50px;"/>
							</div>
						</div>
						@endif						
						<li><input type="file" class="bn file" name="brand_logo" value=""/></li>
					</div>
				</div>

				<div class="row">
					<label>官网</label>
					<div class="rl">
						<input type="text" name="website" placeholder="例：www.google.com" value="{{$brand->website}}"/>
					</div>
				</div>	
				
				
				<div class="row">
					<label>区域</label>
					<div class="rl">
						<input type="text" name="area" value="{{$brand->area}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>详细地址</label>
					<div class="rl">
						<textarea maxlength="100" name="address" >{{$brand->address}}</textarea>
					</div>
				</div>	


				<div class="row">
					<label>推荐</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_hot" @if($brand->is_hot == '0') checked="checked"  @endif 	value="0"  /><font>是</font>
						<input type="radio" class="cb" name="is_hot" @if($brand->is_hot == '1') checked="checked"  @endif 	value="1"  /><font>否</font>
					</div>
				</div>	

				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" name="sort_order" value="{{$brand->sort_order}}"/>
					</div>
				</div>
				
				
				<div class="row">
					<label>联系电话</label>
					<div class="rl">
						<input type="text" name="telephone" value="{{$brand->telephone}}"/>
					</div>
				</div>
				
								
				<div class="row">
					<label>品牌信息</label>
					<div class="rl">
						<textarea  class="desc" name="desc" >{{$brand->desc}}</textarea>
					</div>
				</div>
				
		
				<div class="row">
					<label>型号</label>
					<div class="rl">
						@if(!$models->isEmpty())
						<div class="model_box">
							@foreach($models as $m)
								<span contenteditable="true" id="{{$m->model_id}}" class="models">{{$m->model}}</span>
							@endforeach
						</div>
						@endif
						<textarea class="model" name="model" >{{$brand->model}}</textarea>
						<b>如添加多个型号,每行一个。回车键换行。</b>
					</div>
				</div>	
		
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>

		<script>
		    $('.main_form .row .rl div .img_box img').click(function(){
		    	if($(this).css('width') == '120px')
		    	{
		    		$(this).css({'width':'360','height':'150'});	
		    	}
		    	else
		    	{
		    		$(this).css({'width':'120','height':'50'});
		    	}
				
		    });

		    $('.model_box .models').blur(function(){
		    	var tthis 	= $(this);
		    	var	url		= '{{action('Admin\BrandController@modelUpdate')}}';
		    	if( tthis.html() === '')
		    	{
		    		alert('不能为空!');
		    		return;
		    	}
		    	var data    = {'model':tthis.html(),'model_id':tthis.attr('id'),'_token':'{{csrf_token()}}'};
		    	$.post(url,data,function(res){
		    		if(res.error != 0)
		    		{
		    			alert(res.content);
		    		}
		    	},'json');
		    });


		</script>
		
	</body>

</html>

