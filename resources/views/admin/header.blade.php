<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{$title}}</title>
		<link rel="shortcut icon" href="{{asset('system/images/favicon.ico')}}" type="image/x-icon"/>
		<link rel="stylesheet" type="text/css" href="{{asset('system/css/style.css')}}"/>
		<link rel="stylesheet" href="{{asset('../css/font-awesome.min.css')}}">
		<script src="{{asset('system/js/jquery.js')}}"></script>
		<script src="{{asset('system/js/common.js')}}"></script>
		<script src="{{asset('../date/date.js')}}"></script>
		@include('vendor.ueditor.assets')
	</head>

	<body class="warp">
