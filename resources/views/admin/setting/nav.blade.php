@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/navAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.nav_add')}}</a>
			</h3>

			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>编号</th>
						<th>名称</th>
						<th>显示</th>
						<th>推荐</th>
						<th>新窗口打开</th>
						<th>排序</th>
						<th>操作</th>
					</tr>
					@forelse($data as $v)
					<tr>
						<td><i class="num">{{$v->nav_id}}</i></td>
						<td><a href="{{$v->url}}" target="_blank">{{$v->nav_name}}</a></td>
						<td>@if($v->is_show =='0') <font class="green">是</font> @else <font class="red">否</font> @endif</td>
						<td>@if($v->is_hot =='0') <font class="green">是</font> @else <font class="red">否</font> @endif</td>
						<td>@if($v->is_blank =='0') <font class="green">是</font> @else <font class="red">否</font> @endif</td>
						<td><i class="num">{{$v->sort_order}}</i></td>
						<td><a href="{{$v->url}}" target="_blank" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/navUpdate',[$v->nav_id])}}">编辑</a>|<a class="fa fa-times del" aria-hidden="true" onclick="if(confirm('确认删除吗？')){window.location.href='{{url('admin/navDel',[$v->nav_id])}}'}">删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		

		



	</body>
	<!--<script>
		$('.del').click(function(){
			if(confirm('确定删除吗?'))
			{
				var tthis = $(this);
		    	var url		=	'{{action('Admin\RoleController@del')}}';
				var data	=   {'id':tthis.attr('role'),'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
					else
					{
						alert(res.content);
					}
				},'json');				
			}

		});
	</script>-->
</html>
