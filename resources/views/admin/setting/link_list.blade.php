@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/linkAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.link_add')}}</a>
			</h3>

			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>编号</th>
						<th>名称</th>
						<th>显示</th>
						<th>排序</th>
						<th>操作</th>
					</tr>
					@forelse($data as $v)
					<tr>
						<td><i class="num">{{$v->l_id}}</i></td>
						<td><a target="_blank" href="{{$v->url}}">{{$v->link_name}}</a></td>
						<td>@if($v->is_show =='0') <font class="green">是</font> @else <font class="red">否</font> @endif</td>
						<td><i class="num">{{$v->sort_order}}</i></td>
						<td><a href="{{$v->url}}" target="_blank"  class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/linkUpdate',[$v->l_id])}}">编辑</a>|<a class="fa fa-times del" aria-hidden="true" onclick="if(confirm('确认删除吗？')){window.location.href='{{url('admin/linkDel',[$v->l_id])}}'}"	>删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '')
			<div class="pager f_r">
				{!!$data->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	
		

	</body>

</html>
