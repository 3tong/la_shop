@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>发送者</label>
					<div class="rl">
						<input  type="text" name="sender" value="{{old('sender')}}" placeholder="例：Jonny"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>主题</label>
					<div class="rl">
						<input  class="mid" type="text"  name="title" value="{{old('title')}}" placeholder="例：生日快乐！"/>	
					</div>
				</div>
				
				<div class="row">
					<label>目标邮箱</label>
					<div class="rl">
						<input  class="mid" type="text" name="to" value="{{old('to')}}" placeholder="例：example@example.com"/>
						<b>*</b>		
					</div>
				</div>

				<div class="row">
					<label>邮件内容</label>
					<div class="rl">
						<textarea name="content" class="desc" placeholder="例：生日快乐，给你打了520万人民币。">{{old('desc')}}</textarea>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>注意</label>
					<div class="rl">
						<li>邮件发送目前仅支持SMTP</li>
					</div>
				</div>

								
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="发送" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>																		
			</form>			
		</div>


		
	</body>

</html>

