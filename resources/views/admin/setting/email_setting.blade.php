@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>发送服务器地址</label>
					<div class="rl">
						<input class="mid" type="text" name="host" value="{{$sets->host}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>加密方式</label>
					<div class="rl">
						<input type="radio" class="cb" name="encrytion" value="ssl" @if($sets->encrytion =='ssl') checked="checked" @endif/><font>SSL</font>
						<input type="radio" class="cb" name="encrytion" value="tls" @if($sets->encrytion =='tls') checked="checked" @endif/><font>TLS</font>
					</div>
				</div>	
				
				<div class="row">
					<label>服务器端口</label>
					<div class="rl">
						<input  class="mini" type="text" name="port" value="{{$sets->port}}"/>	
					</div>
				</div>
				
				
				<div class="row">
					<label>邮件发送帐号</label>
					<div class="rl">
						<input  class="mid" type="text" name="username" value="{{$sets->username}}"/>
						<b>*</b>	
					</div>
				</div>

				<div class="row">
					<label>帐号密码</label>
					<div class="rl">
						<input  class="mid" type="password" name="password" value="{{$sets->password}}"/>
						<b>*</b>	
					</div>
				</div>


				<div class="row">
					<label>注意</label>
					<div class="rl">
						<li>1. 邮件发送目前仅支持SMTP</li>
						<li>2. 加密方式和端口号一般不需要修改</li>
					</div>
				</div>

								
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="保存" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>																		
			</form>			
		</div>


		
	</body>

</html>

