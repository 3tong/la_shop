@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>站点名称</label>
					<div class="rl">
						<input class="kw" type="text" name="site_name" value="{{$sets->site_name}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>SEO关键字</label>
					<div class="rl">
						<input  class="kw" type="text" name="site_keywords" value="{{$sets->site_keywords}}"/>	
					</div>
				</div>
				
				
				<div class="row">
					<label>站点描述</label>
					<div class="rl">
						<textarea  class="desc" name="site_desc">{{$sets->site_desc}}</textarea>
					</div>
				</div>

				<div class="row">
					<label>备案号</label>
					<div class="rl">
						<input type="text" name="icp" value="{{$sets->icp}}"/>	
					</div>
				</div>


				<div class="row">
					<label>站点Logo</label>
					<div class="rl">
						@if($sets->logo != '')
						<div>
							<div class="img_box">
								<img id="b_l_l" src="{{asset($sets->logo)}}"  style="width:auto;"/>
							</div>
						</div>
						@endif				
						<li><input type="file" class="bn file" name="logo" value=""/></li>
					</div>
				</div>
				
				
				<div class="row">
					<label>前台分页</label>
					<div class="rl">
						<input type="text" class="mini" name="front_page_size" value="{{$sets->front_page_size}}"/>
						<font>建议不大于100</font><b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>后台分页</label>
					<div class="rl">
						<input type="text" class="mini" name="admin_page_size" value="{{$sets->admin_page_size}}"/>
						<font>建议不大于100</font><b>*</b>		
					</div>
				</div>
		
		
				<div class="row">
					<label>商品水印</label>
					<div class="rl" id="imglist">
						@if($sets->watermark != '')
						<div>
							<div class="img_box">
								<img id="b_l_l" src="{{asset($sets->watermark)}}" style="width:auto;"/>
							</div>
						</div>
						@endif		
						<li><input type="file" class="bn file" name="watermark" value=""/></li>
					</div>
				</div>
				
				<div class="row">
					<label>水印显示</label>
					<div class="rl">
						<input type="radio" class="cb" name="wm_switch" value="0" @if($sets->wm_switch=='0')checked="checked"@endif/><font>是</font>
						<input type="radio" class="cb" name="wm_switch" value="1" @if($sets->wm_switch=='1')checked="checked"@endif/><font>否</font>
					</div>
				</div>	
					
				
				<div class="row">
					<label>联系电话</label>
					<div class="rl">
						<input type="text" name="telephone" value="{{$sets->telephone}}"/>
					</div>
				</div>
				
				<div class="row">
					<label>QQ</label>
					<div class="rl">
						<input type="text" name="qq" value="{{$sets->qq}}"/>
					</div>
				</div>
				
				
				<div class="row">
					<label>邮箱</label>
					<div class="rl">
						<input type="text" name="email" value="{{$sets->email}}"/>
					</div>
				</div>
				
				<div class="row">
					<label>公司名称</label>
					<div class="rl">
						<input type="text" name="com_name" value="{{$sets->com_name}}"/>
					</div>
				</div>
				
				
				<div class="row">
					<label>联系地址</label>
					<div class="rl">
						<textarea  class="address" name="address">{{$sets->address}}</textarea>
					</div>
				</div>	
				
				
				<div class="row">
					<label>网站公告</label>
					<div class="rl">
						<textarea  class="desc" name="notice">{{$sets->notice}}</textarea>
					</div>
				</div>
				

								
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="保存" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>																		
			</form>			
		</div>


		
	</body>

</html>

