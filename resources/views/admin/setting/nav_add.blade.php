@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/nav')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				
				<div class="row">
					<label>商品栏目</label>
					<div class="rl">
						<select>
							<option value="">请选择...</option>
							@foreach($cates as $c)
							<option   nav_name="{{$c->cat_name}}" value="{{url('category',[$c->cat_id])}}">{{str_repeat('&emsp;',$c->level)}}{{$c->cat_name}}</option>
							@endforeach
						</select>				
					</div>
				</div>
				
				<div class="row">
					<label>文章栏目</label>
					<div class="rl">
						<select>
							<option value="">请选择...</option>
							@foreach($artcates as $c)
							<option   nav_name="{{$c->cat_name}}" value="{{url('articlecate',[$c->cat_id])}}">{{str_repeat('&emsp;',$c->level)}}{{$c->cat_name}}</option>
							@endforeach
						</select>				
					</div>
				</div>
				
				
				<div class="row">
					<label>名称</label>
					<div class="rl">
						<input type="text" name="nav_name" value="{{old('nav_name')}}"/>
						<b>*</b>	
					</div>
				</div>
				
				
				<div class="row">
					<label>Url</label>
					<div class="rl">
						<input type="text" class="mid" name="url"  value="{{old('url')}}"/>
						<b>*</b>
					</div>
				</div>	
				
				<div class="row">
					<label>显示</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_show" value="0" checked="checked"/><font>是</font>
						<input type="radio" class="cb" name="is_show" value="1" /><font>否</font>
					</div>
				</div>	

	
				<div class="row">
					<label>推荐</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_hot" value="0" /><font>是</font>
						<input type="radio" class="cb" name="is_hot" value="1" checked="checked" /><font>否</font>
					</div>
				</div>	
				
				<div class="row">
					<label>新窗口打开</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_blank" value="0" /><font>是</font>
						<input type="radio" class="cb" name="is_blank" value="1" checked="checked"/><font>否</font>
					</div>
				</div>	
				
				
				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" value="0" name="sort_order"/>	
					</div>
				</div>

				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input type="hidden" name="cat_id" value="" />
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>

		<script>
			$('.row .rl select').on('change',function(){
				$('input[name=url]').val($(this).val());
				$('input[name=nav_name]').val($(this).find("option:selected").attr('nav_name'));
			});
		</script>
		
	</body>

</html>

