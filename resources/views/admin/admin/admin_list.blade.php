@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/adminAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.admin_add')}}</a>
			</h3>
			<form action="" method="get" class="search">
				<li>
					<b>角色&nbsp;:&nbsp;</b>
					<select name="role_id">
						<option value="">全部</option>
						@foreach($roles as $k=>$v)
							<option value="{{$v->role_id}}" @if($v->role_id == $role_id) selected="selected" @endif>{{$v->role_name}}</option>
						@endforeach
					</select>
				</li>
				<li><input type="submit" value="查询" class="submit"/></li>
			</form>
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>编号</th>
						<th>用户名</th>
						<th>角色组</th>
						<th>邮箱</th>
						<th>上次登录时间</th>
						<th>上次登录ip</th>
						<th>操作</th>
					</tr>
					@forelse($data as $a)
						<tr>
							<td><i class="num">{{$a->id}}</i></td>
							<td><a href="">{{$a->name}}</a></td>
							<td>{{$a->role_name}}</td>
							<td>{{$a->email}}</td>
							<td>@if($a->login_at){{date('Y年n月j日 H:i:s',$a->login_at)}}@endif </td>
							<td><a href="https://www.baidu.com/baidu?wd={{$a->last_ip}}" target="_blank"> {{$a->last_ip}}</a></td>
							<td><a href="" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/adminUpdate',[$a->id])}}">编辑</a>|<a class="fa fa-times del" aria-hidden="true"  admin="{{$a->id}}">删除</a></td>
						</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '')
			<div class="pager f_r">
				{!!$data->appends(['role_id'=>$role_id])->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="hidden" name="role_id" value="{{$role_id}}"/>
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	
		



	</body>
	<script>
		$('.del').click(function(){
			if(confirm('确定删除吗?'))
			{
				var tthis = $(this);
		    	var url		=	'{{action('Admin\AdminController@del')}}';
				var data	=   {'id':tthis.attr('admin'),'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
					else
					{
						alert(res.content);
					}
				},'json');				
			}

		});
	</script>
</html>
