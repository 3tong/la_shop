@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/admin')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				
				<div class="row">
					<label>角色</label>
					<div class="rl">
						<select name="role_id">
							<option value="">请选择...</option>
							@foreach($roles as $r)
								<option @if($admin->role_id == $r->role_id ) selected="selected" @endif value="{{$r->role_id}}">{{$r->role_name}}</option>
							@endforeach
						</select>
						<b>*</b>						
					</div>
				</div>
				
				
				<div class="row">
					<label>用户名</label>
					<div class="rl">
						<input type="text" name="name" value="{{$admin->name}}"/>
						<b>*</b>	
					</div>
				</div>
				

				
				<div class="row">
					<label>邮箱</label>
					<div class="rl">
						<input type="text" name="email" value="{{$admin->email}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>密码</label>
					<div class="rl">
						<input type="password" name="password" 	id="password" value="{{old('password')}}"/>
						<b>*</b>	
					</div>
				</div>

				
				<div class="row">
					<label>重复密码</label>
					<div class="rl">
						<input type="password" name="password_confirmation"  id="password_confirmation" value="{{old('password_confirmation')}}"/>
						<b>*</b>	
					</div>
				</div>
				
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>

		<script>

		</script>
		
	</body>

</html>

