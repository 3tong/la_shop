<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{$title}}</title>
		<link rel="stylesheet" type="text/css" href="{{asset('system/css/style.css')}}"/>
		<link rel="stylesheet" href="{{asset('../css/font-awesome.min.css')}}">
		<script src="{{asset('system/js/jquery.js')}}"></script>
		<script src="{{asset('system/js/common.js')}}"></script>
		<script src="{{asset('../date/date.js')}}"></script>
		@include('vendor.ueditor.assets')
	</head>

	<body class="login_bg">
		<div class="login_box">
			<h2>{{$title}}</h2>

			<form action="" method="post">
				<div class="input_box">
					<div class="row">
						<div class="icon">
							<i class="fa fa-user" aria-hidden="true"></i>
						</div>

						<input placeholder="用户名" name="name" type="text" value="{{old('name')}}"/>
					</div>

					<div class="row">
						<div class="icon">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</div>
						<input placeholder="密&emsp;码" name="password" type="password" value="{{old('password')}}"/>
					</div>
			
					<div class="row">
						<div class="icon">
							<i class="fa fa-image" aria-hidden="true"></i>
						</div>
						<input class="yzm" placeholder="验证码" name="captcha" type="text" value="{{old('captcha')}}"/>
						<img style="vertical-align: middle;" src="{{captcha_src('admin')}}" alt="验证码" onclick="this.src=this.src+Math.random();"/>
					</div>
				</div>
				
				
				@if(count($errors)>0)
					<ul style=" margin-top: -24px;">
				 	@if(is_object($errors))
						@foreach($errors->all() as $error)
							<li style="line-height:20px; color: #F10214;">{{$error}}</li>
						@endforeach
			        @else
        				<li style="line-height:20px; color: #F10214;">{{$errors}}</li>
					@endif
					</ul>
				@endif

				{!!csrf_field()!!}
				<input type="submit" class="sub" value="登录"/>
			</form>
		</div>
		
	</body>

</html>
