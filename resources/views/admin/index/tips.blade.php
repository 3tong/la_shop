@include('admin.header')

		<div class="main_form">
			<h3>
				<font><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;{{$title}}</font>
			</h3>

			<div class="mb_25"></div>

			@if($act == 'succ' || $act == 'update_succ')
			<div class="row green f_25 mb_25">
				<label><i class="fa fa-check-circle" aria-hidden="true"></i></label>
				<div class="rl">{{$info}}</div>
			</div>
			@else
	 		<div class="row red f_25 mb_25">
				<label><i class="fa fa-times-circle" aria-hidden="true"></i></label>
				<div class="rl">{{$info}}</div>
			</div>
			@endif

			<div class="mb_25"></div>
			
			@if($act == 'update_succ')
			<div class="row">
				<label><i class="fa fa-list" aria-hidden="true"></i></label>
				<div class="rl"><span id="timer">{{$sec}}</span>秒后<a href="{{$url}}">返回首页</a></div>
			</div>
			@elseif($act == 'succ')
			<div class="row">
				<label><i class="fa fa-list" aria-hidden="true"></i></label>
				<div class="rl"><span id="timer">{{$sec}}</span>秒后<a href="{{$url}}">返回列表</a></div>
			</div>
			@else
			<div class="row">
				<label><i class="fa fa-list" aria-hidden="true"></i></label>
				<div class="rl"><span id="timer">{{$sec}}</span>秒后<a href="javascript:window.history.go(-1);">返回上一页</a></div>
			</div>
			@endif
			
			@if($act == 'succ'  || $act == 'update_succ')
			<div class="row">
				<label><i class="fa fa-hand-o-left" aria-hidden="true"></i></label>
				<div class="rl"><a style="cursor:pointer;" onclick="window.history.go(-1);">继续操作</a></div>
			</div>
			@else
			<div class="row">
				<label><i class="fa fa-hand-o-left" aria-hidden="true"></i></label>
				<div class="rl"><a style="cursor:pointer;" href="{{$url}}">返回首页</a></div>
			</div>
			@endif
			
			
		</div>

	</body>
	<script>
		@if($act == 'succ' || $act == 'update_succ')
		$(function(){
			var timer = $('#timer').html()*1;
	       	mycount = setInterval(count,1000);
            function count()
            {
				timer--;
				$("#timer").html(timer);
				if(timer == 0)
				{
					window.location.href="{{$url}}";	
				}
            }

		})
		@else
			$(function(){
			var timer = $('#timer').html()*1;
	       	mycount = setInterval(count,1000);
            function count()
            {
				timer--;
				$("#timer").html(timer);
				if(timer == 0)
				{
					window.history.go(-1);
				}
            }
		})	
		@endif
	</script>
</html>
