@include('admin.header')

		<!--        top         -->
		    <div class="top _back f_l">
		    		<a href="{{url('admin')}}">
			    	    <div class="logo f_l">
			    	  		<img src="{{asset('./system/images/w200.png')}}" />	
			    	    </div>
		    	    </a>	
		    	    <div class="f_l nav">
		    	    	<span><font class="sw" id="close"><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;<b>关闭</b></font></span>
		    	    	<span><font class="sw" id="back"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;<b>返回</b></font></span>
		    	    	<span><font class="sw" id="return"><b>前进</b>&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></font></span>
		    	    	<span><font class="sw" id="refresh"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;<b>刷新</b></font></span>
		    	    	<a href="{{url('admin/item')}}" 	target="main" >商品列表</a>
		    	    	<a href="{{url('admin/brand')}}"	target="main" >品牌列表</a>
		    	    	<a href="{{url('admin/company')}}"  target="main" >企业列表</a>
		    	    	<a href="{{url('admin/article')}}"  target="main" >文章列表</a>
		    	    	<a href="{{url('admin/commonSetting')}}"  	target="main" >通用设置</a>
		    	    	<a href="{{url('admin/itemBatchUpload')}}"  	target="main" >商品批量上传</a>
		    	    	<a href="{{url('admin/companyBatchUpload')}}"  	target="main" >企业批量上传</a>
		    	    	<a href="{{url('admin/articleBatchUpload')}}"  	target="main" >文章批量上传</a>
		    	    </div>	    	  
		    	    <div class="userinfo f_r">
		    	    	<span><i class="fa fa-user" aria-hidden="true"></i>&nbsp;{{session('admin.role_name')}}&nbsp;:&nbsp;{{session('admin.admin_name')}}</span>
		    	  		<a onclick="if(confirm('确定退出吗？')){window.location.href='{{url('admin/logout')}}';}"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;注销</a>
		    	    </div>	
		    </div>
		<!--        top         -->
		    
		<!--         mainleft          -->		
			<div class="main_left">
				<ul class="menu">
					<li>
						<h3 class="off">商品管理</h3>
						<ul>
							<a href="{{url('admin/itemCate')}}" target="main">分类列表</a>
							<a href="{{url('admin/itemCateAdd')}}" target="main">分类添加</a>
							<a href="{{url('admin/item')}}" target="main">{{trans('admin.item_list')}}</a>
							<a href="{{url('admin/itemAdd')}}" target="main">{{trans('admin.item_add')}}</a>
							<a href="{{url('admin/itemBin')}}" target="main">{{trans('admin.item_bin')}}</a>
						</ul>
					</li>	
					<li>
						<h3 class="off">订单管理</h3>
						<ul>
							<a href="#">商品列表</a>
							<a href="#">添加商品</a>
						</ul>
					</li>
					<li>
						<h3 class="off">品牌管理</h3>
						<ul>
							<a href="{{url('admin/brand')}}" target="main">品牌列表</a>
							<a href="{{url('admin/brandAdd')}}" target="main">品牌添加</a>
						</ul>					
					</li>

					<li>
						<h3 class="off">企业管理</h3>
						<ul>
							<a href="{{url('admin/company')}}" target="main">企业列表</a>			
						</ul>					
					</li>
										
					<li>
						<h3 class="off">文章列表</h3>
						<ul>
							<a href="{{url('admin/articleCate')}}" target="main">分类列表</a>
							<a href="{{url('admin/articleCateAdd')}}" target="main">分类添加</a>
							<a href="{{url('admin/article')}}" target="main">文章列表</a>
							<a href="{{url('admin/articleAdd')}}" target="main">文章添加</a>
							<a href="{{url('admin/articleBin')}}" target="main">文章回收站</a>
						</ul>					
					</li>


					<li>
						<h3 class="off">权限系统</h3>
						<ul>
							<a href="{{url('admin/role')}}" target="main">角色列表</a>
							<a href="{{url('admin/roleAdd')}}" target="main">角色添加</a>
							<a href="{{url('admin/admin')}}" target="main">管理员列表</a>
							<a href="{{url('admin/adminAdd')}}" target="main">管理员添加</a>
						</ul>
					</li>
					
					
					<li>
						<h3 class="off">会员管理</h3>
						<ul>
							<a href="#">商品列表</a>
							<a href="#">添加商品</a>
						</ul>
					</li>
					
					<li>
						<h3 class="off">数据备份</h3>
						<ul>
							<a href="#">商品列表</a>
							<a href="#">添加商品</a>
						</ul>
					</li>


					<li>
						<h3 class="off">广告系统</h3>
						<ul>
							<a href="{{url('admin/adp')}}" target="main">广告位列表</a>
							<a href="{{url('admin/adpAdd')}}" target="main">广告位添加</a>
							<a href="{{url('admin/ad')}}" target="main">广告列表</a>
							<a href="{{url('admin/adAdd')}}" target="main">广告添加</a>
						</ul>
					</li>
					<li>
						<h3 class="off">系统设置</h3>
						<ul>
							<a href="{{url('admin/commonSetting')}}" target="main">通用设置</a>
							<a href="{{url('admin/emailSetting')}}" target="main">邮件设置</a>
							<a href="{{url('admin/emailTest')}}" target="main">邮件发送测试</a>
							<a href="{{url('admin/link')}}" target="main">友链设置</a>
							<a href="{{url('admin/nav')}}" target="main">导航设置</a>

						</ul>
					</li>	
				</ul>

			</div>
        <!--         mainleft          -->		
        
        
        
        <!--         mainright          -->
        <div class="main_right">
			<iframe src="{{url('admin/info')}}" frameborder="0" width="100%" height="100%" name="main"></iframe> 
        </div>
        <!--         mainright          -->		

		<!--         footer          -->
		<div class="footer">
		Written By Jonny. CopyRight © 2017. Powered By Laravel.
		</div>
		<!--         footer          -->		
		
		    


	</body>
	<script>	  		 
	  	$('#close').click(function(){
	  		//alert($('.main_right').css('left'))
	  		if($('.main_right').css('left') == '190px')
	  		{
	  			$(this).find('b').html('开启');
	  			$('.main_left').css('width','0');
	  			$('.main_right').css('left','0');
	  			$('.footer').css('left','0');
	  		}
	  		else
	  		{
	  			$(this).find('b').html('关闭');
	  			$('.main_left').css('width','189px');
	  			$('.main_right').css('left','190px');	
	  			$('.footer').css('left','190px');	
	  		}
	  		
	  	});
	  	
  		$('#back').click(function(){
	  		window.top.frames['main'].window.history.go(-1);
  		});
  		
  		$('#return').click(function(){
	  		window.top.frames['main'].window.history.go(+1);
  		});
  		
  		$('#refresh').click(function(){
	  		window.top.frames['main'].window.location.reload();
  		});  		

	</script>
</html>
