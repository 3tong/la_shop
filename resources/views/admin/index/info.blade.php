@include('admin.header')

		<div class="serverdata">
			<div class="l_box f_l">
				<h3>数据统计</h3>
				<li>
					<span>会&nbsp;员&nbsp;: <a href="#">95271</a></span>
					<span>订&nbsp;单&nbsp;: <a href="#">121211</a></span>
				</li>
				<li>
					<span>企&nbsp;业&nbsp;: <a href="{{url('admin/company')}}">{{$companies}}</a></span>
					<span>商&nbsp;品&nbsp;: <a href="{{url('admin/item')}}">{{$items}}</a></span>
				</li>
				<li>
					<span>广&nbsp;告&nbsp;: <a href="{{url('admin/ad')}}">{{$ads}}</a></span>
					<span>文&nbsp;章&nbsp;: <a href="{{url('admin/article')}}">{{$articles}}</a></span>
				</li>
			</div>
			<div class="r_box f_r">
				<h3>角色信息</h3>
				<dd class="adminfo">
					<b>用&nbsp;&nbsp;户&nbsp;:&nbsp;<i>{{session('admin.admin_name')}}</i></b>
					<b>角&nbsp;&nbsp;色&nbsp;:&nbsp;<i>{{session('admin.role_name')}}</i></b>
				</dd>
				<dd class="adminfo">
					<b>当前登录ip&nbsp;:&nbsp;<i><a href="https://www.baidu.com/baidu?wd={{getIp()}}" target="_blank">{{getIp()}}</a></i></b>
					<b>当前登录时间&nbsp;:&nbsp;<i>{{session('admin.now_login')}}</i></b>
				</dd>
				<dd class="adminfo">
					<b>上次登录ip&nbsp;:&nbsp;<i><a href="https://www.baidu.com/baidu?wd={{session('admin.last_ip')}}" target="_blank">{{session('admin.last_ip')}}</a></i></b>
					<b>上次登录时间&nbsp;:&nbsp;<i>{{session('admin.last_login')}}</i></b>
				</dd>

			</div>
		</div>
		<div class="serverinfo">
			<h3>系统信息</h3>
			<ul>
				<li>
					<span><font class="l_text">操作系统</font>{{PHP_OS}}</span>
					<span><font class="l_text">软件版本</font>{{$ser['SERVER_SOFTWARE']}}</span>
					<span><font class="l_text">数据库</font>{{$ser['DB_CONNECTION']}}</span>
					<span><font class="l_text">网址</font><a target="_blank" href="http://{{$ser['SERVER_NAME']}}">{{$ser['SERVER_NAME']}}</a></span>
				</li>
				<li>
					<span><font class="l_text">服务器IP</font>{{$ser['SERVER_ADDR']}}</span>
					<span><font class="l_text">时区设置</font>{{config('app.timezone')}}</span>
					<span><font class="l_text">POST限制</font>{{ini_get('post_max_size')}}</span>
					<span><font class="l_text">上传限制</font>{{ini_get('upload_max_filesize')}}</span>
				</li>
				<li>
					<span><font class="l_text">运行方式</font>{{$run}}</span>
					<span><font class="l_text">运行最大时长</font>{{ini_get('max_execution_time')}}秒</span>
					<span><font class="l_text">单脚本最大内存</font>{{ini_get('memory_limit')}}</span>
					<span><font class="l_text">报错级别</font>{{ini_get('error_reporting')}}</span>
				</li>
			</ul>
		</div>
	</body>
</html>
