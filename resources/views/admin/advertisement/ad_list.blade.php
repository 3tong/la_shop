@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/adAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.ad_add')}}</a>
			</h3>
			<form action="" method="get" class="search">
				<li>
					<b>广告位置&nbsp;:&nbsp;</b>
					<select name="adp_id">
						<option value="">全部</option>
						@foreach($adps as $k=>$v)
							<option value="{{$v->adp_id}}" @if($v->adp_id == $adp_id) selected="selected" @endif>{{$v->adp_name}}</option>
						@endforeach
					</select>
				</li>
				<li><input type="submit" value="查询" class="submit"/></li>
			</form>			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>编号</th>
						<th>名称</th>
						<th>位置</th>
						<th>显示</th>
						<th>排序</th>
						<th>开始日期</th>
						<th>结束日期</th>
						<th>操作</th>
					</tr>
					@forelse($data as $v)
					<tr>
						<td><i class="num">{{$v->ad_id}}</i></td>
						<td><a href="{{$v->url}}" target="_blank">{{$v->ad_name}}</a></td>
						<td>{{$v->adp_name}}</td>
						<td>@if($v->is_show =='0')<font class="green">是</font>@elseif($v->is_show =='1')<font class="red">否</font>@endif</td>
						<td><i class="num">{{$v->sort_order}}</i></td>
						<td>{{getTime($v->start_time)}}</td>
						<td>{{getTime($v->end_time)}}</td>
						<td><a href="" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/adUpdate',[$v->ad_id])}}">编辑</a>|<a class="fa fa-times" aria-hidden="true"  onclick="if(confirm('确认删除吗？')){window.location.href='{{url('admin/adDel',[$v->ad_id])}}'}">删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '')
			<div class="pager f_r">
				{!!$data->appends(['adp_id',$adp_id])->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="hidden" name="adp_id" 	value="{{$adp_id}}" />
					<input type="number" name="page" 	max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	
		



	</body>
	<script>
		$('.del').click(function(){
			if(confirm('确定删除吗?'))
			{
				var tthis = $(this);
		    	var url		=	'{{action('Admin\AdvertisementController@adpDel')}}';
				var data	=   {'id':tthis.attr('adp_id'),'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
					else
					{
						alert(res.content);
					}
				},'json');				
			}

		});
	</script>
</html>
