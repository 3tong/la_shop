@include('admin.header')

		<div class="main_list">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/adpAdd')}}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;{{trans('admin.adp_add')}}</a>
			</h3>

			
			<table class="list"  border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<th>编号</th>
						<th>广告位</th>
						<th>宽高</th>
						<th>描述</th>
						<th>操作</th>
					</tr>
					@forelse($data as $v)
					<tr>
						<td><i class="num">{{$v->adp_id}}</i></td>
						<td><a href="{{url('admin/ad/').'?adp_id='.$v->adp_id}}">{{$v->adp_name}}</a></td>
						<td>{{$v->width}}px*{{$v->height}}px</td>
						<td>{{$v->desc}}</td>
						<td><a href="{{url('admin/ad/').'?adp_id='.$v->adp_id}}" class="fa fa-search" aria-hidden="true">查看</a>|<a class="fa fa-pencil-square-o" aria-hidden="true" href="{{url('admin/adpUpdate',[$v->adp_id])}}">编辑</a>|<a class="fa fa-times del" aria-hidden="true"  adp_id="{{$v->adp_id}}">删除</a></td>
					</tr>
				    @empty
				    <tr><td class="null" colspan="99"><i class="fa fa-info-circle" aria-hidden="true"></i>没有找到结果</td></tr>
					@endforelse					
			</table>

		</div>
		
		<div class="page_box">
			@if($data != '')
			<div class="pager f_r">
				{!!$data->render()!!}
				<font class="total">{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
				<form action="" method="get">
					<input type="number" name="page" max="{{$data->lastPage()}}" min="1" value="" />
					<input type="submit" class="sub" value="Go"/>
				</form>
			</div>
			@endif	
		</div>	
		



	</body>
	<script>
		$('.del').click(function(){
			if(confirm('确定删除吗?'))
			{
				var tthis = $(this);
		    	var url		=	'{{action('Admin\AdvertisementController@adpDel')}}';
				var data	=   {'id':tthis.attr('adp_id'),'_token':'{{csrf_token()}}'};
				$.post(url,data,function(res){
					if(res.error == 0)
					{
						window.location.reload();
					}
					else
					{
						alert(res.content);
					}
				},'json');				
			}

		});
	</script>
</html>
