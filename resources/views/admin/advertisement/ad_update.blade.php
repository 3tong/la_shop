@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/ad')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				
				<div class="row">
					<label>广告位置</label>
					<div class="rl">
						<select name="adp_id">
							<option value="">请选择...</option>
							@foreach($adps as $k=>$v)
								<option value="{{$v->adp_id}}" @if($v->adp_id == $ad->adp_id) selected="selected" @endif>{{$v->adp_name}}</option>
							@endforeach
						</select>
						<b>*</b>						
					</div>
				</div>
				
				
				
				<div class="row">
					<label>广告名称</label>
					<div class="rl">
						<input type="text" name="ad_name" value="{{$ad->ad_name}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>广告Url</label>
					<div class="rl">
						<input type="text" name="url" value="{{$ad->url}}"/>
						<b>*</b>	
					</div>
				</div>

				<div class="row">
					<label>广告图片</label>
					<div class="rl" id="imglist">
						@if($ad->img != '')
						<div>
							<div class="img_box">
								<img id="b_l_l" src="{{asset($ad->img)}}" style="width:{{$wh->width/5}}px; height:{{$wh->height/5}}px;"/>
							</div>
						</div>
						@endif		
						<li><input type="file" class="bn file" name="img" value=""/></li>
					</div>
				</div>
				
				
				<div class="row">
					<label>显示</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_show" value="0" @if($ad->is_show =='0')checked="checked"@endif /><font>是</font>
						<input type="radio" class="cb" name="is_show" value="1" @if($ad->is_show =='1')checked="checked"@endif /><font>否</font>
					</div>
				</div>	
					
				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" name="sort_order" 	value="{{$ad->sort_order}}"/>
					</div>
				</div>
				
				<div class="row">
					<label>开始日期</label>
					<div class="rl">
						<input placeholder="请输入日期" name="start_time" value="{{getTime($ad->start_time)}}" class="laydate-icon" onclick="laydate()">
						<b>*</b>	
					</div>
				</div>	
				
				<div class="row">
					<label>结束日期</label>
					<div class="rl">
						<input placeholder="请输入日期" name="end_time" value="{{getTime($ad->end_time)}}" class="laydate-icon" onclick="laydate()">
						<b>*</b>	
					</div>
				</div>	
				
				<div class="row">
					<label>联系人</label>
					<div class="rl">
						<input type="text" name="linkman" value="{{$ad->linkman}}"/>	
					</div>
				</div>
				
				<div class="row">
					<label>电话</label>
					<div class="rl">
						<input type="text" name="phone" value="{{$ad->phone}}"/>	
					</div>
				</div>
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>


	</body>
		<script>
		    
		    $('.main_form .row .rl div .img_box img').click(function(){
		    	if($(this).css('width') == '{{$wh->width/5}}px')
		    	{
		    		$(this).css({'width':'{{$wh->width/2}}','height':'{{$wh->height/2}}'});	
		    	}
		    	else
		    	{
		    		$(this).css({'width':'{{$wh->width/5}}','height':'{{$wh->height/5}}'});
		    	}
				
		    });
		</script>
</html>

