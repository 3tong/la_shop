@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/ad')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				
				<div class="row">
					<label>广告位置</label>
					<div class="rl">
						<select name="adp_id">
							<option value="">请选择...</option>
							@foreach($adps as $k=>$v)
								<option value="{{$v->adp_id}}" @if($v->adp_id == old('adp_id')) selected="selected" @endif>{{$v->adp_name}}</option>
							@endforeach
						</select>
						<b>*</b>						
					</div>
				</div>
				
				
				
				<div class="row">
					<label>广告名称</label>
					<div class="rl">
						<input type="text" name="ad_name" value="{{old('ad_name')}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>广告Url</label>
					<div class="rl">
						<input type="text" name="url" value="{{old('url')}}"/>
						<b>*</b>	
					</div>
				</div>
				
				<div class="row">
					<label>广告图片</label>
					<div class="rl" id="imglist">
						<li><input type="file" class="bn file" name="img" value=""/><b>*</b></li>
					</div>
				</div>
				
				
				<div class="row">
					<label>显示</label>
					<div class="rl">
						<input type="radio" class="cb" name="is_show" value="0" checked="checked" /><font>是</font>
						<input type="radio" class="cb" name="is_show" value="1" /><font>否</font>
					</div>
				</div>	
					
				<div class="row">
					<label>排序</label>
					<div class="rl">
						<input type="text" name="sort_order" 	value="@if(!old('sort_order'))0 @else{{old('sort_order')}}@endif"/>
					</div>
				</div>
				
				<div class="row">
					<label>开始日期</label>
					<div class="rl">
						<input placeholder="请输入日期" name="start_time" value="{{old('start_time')}}" class="laydate-icon" onclick="laydate()">
						<b>*</b>	
					</div>
				</div>	
				
				<div class="row">
					<label>结束日期</label>
					<div class="rl">
						<input placeholder="请输入日期" name="end_time" value="{{old('end_time')}}" class="laydate-icon" onclick="laydate()">
						<b>*</b>	
					</div>
				</div>	
				
				<div class="row">
					<label>联系人</label>
					<div class="rl">
						<input type="text" name="linkman" value="{{old('linkman')}}"/>	
					</div>
				</div>
				
				<div class="row">
					<label>电话</label>
					<div class="rl">
						<input type="text" name="phone" value="{{old('phone')}}"/>	
					</div>
				</div>
				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>


	</body>

</html>

