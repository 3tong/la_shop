@include('admin.header')

		@if(count($errors)>0)
		<div id="warning_box">
			<h3><i id="close" class="fa fa-times" aria-hidden="true"></i></h3>
			<ol>
			@foreach($errors->all() as $error)<li>{{$error}}</li>@endforeach
			</ol>
		</div>
		<div id="mask"></div>
		<script>
			$("#close,#mask").click(function(){
				$('#warning_box,#mask').hide();
			});
		</script>
		@endif
		
		
		<div class="main_form">
			<h3>
				<font>{{$title}}</font>
				<a href="{{url('admin/adp')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;返回列表</a>
			</h3>
			<form action=""  method="post" enctype="multipart/form-data">
				<div class="row">
					<label>广告位</label>
					<div class="rl">
						<input type="text" name="adp_name" value="{{$adp->adp_name}}"/>
						<b>*</b>	
					</div>
				</div>
				<div class="row">
					<label>宽</label>
					<div class="rl">
						<input type="text" name="width" value="{{$adp->width}}"/>
						<font>px(像素)</font>
						<b>*</b>	
					</div>
				</div>
				<div class="row">
					<label>高</label>
					<div class="rl">
						<input type="text" name="height" value="{{$adp->height}}"/>
						<font>px(像素)</font>
						<b>*</b>	
					</div>
				</div>
				<div class="row">
					<label>描述</label>
					<div class="rl">
						<textarea  class="desc" maxlength="200" name="desc">{{$adp->desc}}</textarea>
					</div>
				</div>
	


				
				<div class="row">
					<label></label>
					<div class="rl">
						{!!csrf_field()!!}
						<input class="sub" id="sub" type="submit" value="提交" />
						<input class="sub" type="reset" value="清空" />
						<b>带‘*’的为必填项。</b>							
					</div>
				</div>				
						
			</form>			
		</div>


	</body>

</html>

