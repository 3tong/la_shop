<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{$title}}</title>
<meta name="keywords" content="{{CS('site_keywords')}}">
	<meta name="description" content="{{CS('site_desc')}}">
<link href="{{asset('home/css/style.css')}}" rel="stylesheet" type="text/css" />
<script src="{{asset('home/js/jquery.js')}}"></script>
</head>
<body>
<!--头部-->
<div id="header">
  <div class="top">
    <div class="topT">
      <div class="loginh"><a href="登陆.html" target="_blank" class="on">请登录</a><a href="注册.html" target="_blank">免费注册</a></div>
      <div class="time">今天是：<span id="divT"></span></div>
      <div class="weather">
        <iframe allowtransparency="true" frameborder="0" width="180" height="36" scrolling="no" src="http://tianqi.2345.com/plugin/widget/index.htm?s=3&z=2&t=0&v=0&d=1&bd=0&k=&f=000000&q=1&e=1&a=1&c=58321&w=180&h=36&align=center"></iframe>
      </div>
      <div class="set"><a href='' onClick="this.style.behavior='url(#default#homepage)'; this.setHomePage('http://www.17sucai.com');">设为首页</a><a href="javascript:window.external.AddFavorite('http://www.17sucai.com','广告企业网');">加入收藏</a></div>
    </div>
  </div>
  <!--搜索-->
  <div class="topM">
    <div class="logo"><a href="/"><img src="{{asset(CS('logo'))}}" /></a></div>
    <div class="searchbox">
      <div class="search_box"> 
        <div class="search">
          <form method="get" action="">
            <input class="enter" placeholder="您想找什么？" name="keywords"  value="">
            <input class="sb" name="Input" type="submit" value="搜索">
          </form>
        </div>
      </div>
	  	<div  class="hotkeywords">热点：
	  		<a href="#" target="_blank">广告资讯</a>
	  		<a href="#" target="_blank">分类广告</a>
	  		<a href="#" target="_blank">供求广告</a>
		</div>
    </div>
  </div>
  <!--顶部导航-->
  <div class="topB">
    <div class="nav">
      <ul>
        <li><a href="/" >首页</a></li>
				@foreach($navs as $v)
		        	<li>
		        	<a @if( $v->is_blank == '0')target="_blank" @endif href="{{$v->url}}">{{$v->nav_name}}</a>
		    		</li>
				@endforeach
      </ul>
    </div>
    
  
</div>

</div>