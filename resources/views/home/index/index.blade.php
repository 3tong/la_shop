@include('home.header')

<div class="g_g_box">
	@foreach($ad_under_nav as $v)
	<div class="item">
		<a href="{{$v->url}}">
			<div class="imgtitle">微信：{{$v->linkman}}</div>
		</a>
		<img src="{{$v->img}}" alt="" />
	</div>
	@endforeach
	@foreach($ad_under_nav as $v)
	<div class="item">
		<a href="{{$v->url}}">
			<div class="imgtitle">微信：{{$v->linkman}}</div>
		</a>
		<img src="{{$v->img}}" alt="" />
	</div>
	@endforeach
</div>

@foreach($tops as $v)
<div class="art_box">
	<div class="ti">
		<h3>{{$v['cat_name']}}</h3>		
	</div>
	<ul>
		@foreach($v['tops'] as $v)
		<li><a style="cursor: pointer;" href="{{url('article',[$v['art_id']])}}" target="_blank">{{str_limit($v['title'],35)}}</a><b>&nbsp;微信：{{$v['wechat']}}</b></li>
		@endforeach

	</ul>
</div>
@endforeach



<script>
	$('.g_g_box .item').mouseover(function(){
		$(this).find('.imgtitle').show();
	});
	
	$('.g_g_box .item').mouseout(function(){
		$(this).find('.imgtitle').hide();
	});
</script>

@include('home.footer')