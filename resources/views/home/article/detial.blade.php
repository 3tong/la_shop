@include('home.header')
<!--所在位置-->
<div class="location">
  <div class="postion">
    <div class="wz">您当前的位置： <a href="{{url('/')}}">首页</a> > <a href="{{url('/articlecate',[$cate->cat_id])}}">{{$cate->cat_name}}</a> > <span>{{$detail->title}}</span></div>
  </div>
</div>
<!--广告-->
<div class="clear_0"></div>
<!--中间内容-->
<div class="mainsub">
  <!--左侧内容-->
  <div class="left702">

		<h1 class="cen">{{$detail->title}}</h1>
    <div class="titleinfo">发布时间：{{getTime($detail->create_at)}}&emsp;&emsp;点击：{{$detail->click}}&emsp;&emsp;作者：{{$detail->editor}}</div>
		
		@if($detail->wechat != '' || $detail->qq!= '' || $detail->phone !='')
		<div class="gaiyao">
				<ul>
					<li>微&nbsp;信&nbsp;:{{$detail->wechat}}</li>
					<li>Q&nbsp;Q&nbsp;:{{$detail->qq}}</li>
					<li>手&nbsp;机&nbsp;:{{$detail->phone}}</li>
				</ul>
    </div>
 		@endif
    <div class="content_news">
    	{!!$detail->content!!}
    </div>
		


		
  </div>
  
  <!--右侧内容-->
  <div class="right278">
    <!--最新供求信息-->
    @if(!$hots->isEmpty())
    <div class="zxgqxx">
      <div class="title6">
        <h1>热门信息</h1>
      </div>
      @foreach($hots as $v)
      <div class="latest">
        <div class="leibie">热门</div>
        <div class="l_infor">
          <div class="l_title"><a href="{{url('/article',[$v->art_id])}}">{{str_limit($v->title,50)}} </a></div>
          <div class="l_gs">微信：{{$v->wechat}}</div>
        </div>
      </div>
			@endforeach
    </div>
    @endif
    
  </div>
</div>

@include('home.footer')
