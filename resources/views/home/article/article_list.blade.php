@include('home.header')
<!--所在位置-->
<div class="location">
  <div class="postion">
    <div class="wz">您当前的位置： <a href="{{url('/')}}">首页</a> > <span>{{$cate->cat_name}}</span> </div>
  </div>
</div>
<!--广告-->
<div class="clear_0"></div>
<!--中间内容-->
<div class="mainsub">
  <!--左侧内容-->
  <div class="left702">

    <div class="supply">
      <div class="title11">
        <h1>{{$cate->cat_name}}</h1>
      </div>
      <!--信息列表-->
      

      @foreach($data as $v)
      <div class="list5">
        <div class="title14">
          <h2><a href="{{url('/article',[$v->art_id])}}" target="_blank">{{$v->title}}</a></h2>
        </div>
        <div class="list_con">{{str_limit($v->desc,100)}}</div>
        <div class="fbtime">
        	<span style="float:left;">发布时间：{{getTime($v->create_at)}}&emsp;微信：{{$v->wechat}}</span>
        	<span style="float:right;">点击量：{{$v->click}}</span>
      	</div>
      </div>
      @endforeach
      
      
     
			@if($data != '')
			<div class="pager f_r">
				{!!$data->render()!!}
				<font>{{$data->currentPage()}}/{{$data->lastPage()}}页&nbsp;共{{$data->total()}}条结果</font>
			</div>
			@endif	

   
    </div>
    
    
    
  </div>
  <!--右侧内容-->
  <div class="right278">
    <!--最新供求信息-->
    @if(!$hots->isEmpty())
    <div class="zxgqxx">
      <div class="title6">
        <h1>热门信息</h1>
      </div>
      
      @foreach($hots as $v)
      <div class="latest">
        <div class="leibie">热门</div>
        <div class="l_infor">
          <div class="l_title"><a href="{{url('/article',[$v->art_id])}}">{{str_limit($v->title,50)}} </a></div>
          <div class="l_gs">微信：{{$v->wechat}}</div>
        </div>
      </div>
			@endforeach
    </div>
		@endif
  </div>
</div>

@include('home.footer')