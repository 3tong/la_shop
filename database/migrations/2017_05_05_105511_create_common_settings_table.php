<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('common_settings', function (Blueprint $table) {
        	$table->increments('id');
            $table->string('site_name');
            $table->string('site_keywords');
            $table->string('site_desc');
            $table->string('icp');
            $table->string('logo');
            $table->smallInteger('front_page_size')->default(20);
            $table->smallInteger('admin_page_size')->default(20);
            $table->string('watermark');
            $table->string('wm_switch')->default(1);
            $table->string('telephone');
            $table->string('qq');
        	$table->string('email');
            $table->string('com_name');
            $table->string('address');
            $table->string('notice');
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('common_settings');
    }
}
