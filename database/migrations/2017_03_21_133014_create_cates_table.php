<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cates', function (Blueprint $table) {
            $table->increments('cat_id');
            $table->integer('parent_id')->default('0');
            $table->string('cat_name', 100);
            $table->string('desc', 100);
			$table->tinyInteger('is_show')->default('0');            
            $table->string('unit', 10);
            $table->integer('sort_order')->default('0');
            $table->string('keyword');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cates');
    }
}
