<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('item_id');
			$table->string('item_name',100);
			$table->string('item_sn',30);
			$table->integer('cat_id');
            $table->integer('brand_id');
			$table->decimal('price',18, 2);
			$table->decimal('market_price',18, 2);
			$table->integer('com_id');
			$table->integer('number')->unsigned();
			$table->integer('model_id');
			$table->integer('province_id');
            $table->integer('city_id');
			$table->integer('area_id');
			$table->tinyInteger('status');
            $table->tinyInteger('is_show')->default('0');
			$table->tinyInteger('is_sale')->default('0');
			$table->tinyInteger('is_hot')->default('1');
            $table->integer('sort_order')->default('0');
			$table->string('keyword');
			$table->text('desc');
			$table->integer('click')->default('0');
			$table->integer('sales_number')->default('0');
         	$table->string('create_at',20);
			$table->string('update_at',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
