<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('brand_id');
            $table->string('brand_name',100);
            $table->string('brand_logo',100);
            $table->string('desc');
            $table->tinyInteger('is_hot')->default('1');
            $table->integer('sort_order')->default('0');
            $table->string('area',20);
            $table->string('website');
            $table->string('address');
            $table->string('telephone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brands');
    }
}
