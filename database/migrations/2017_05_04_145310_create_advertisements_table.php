<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->increments('ad_id');
            $table->integer('adp_id');
            $table->string('ad_name',80);
            $table->string('url');
            $table->string('img');
            $table->tinyInteger('is_show')->default('0');
            $table->integer('sort_order')->default('0');
            $table->string('start_time',20);
            $table->string('end_time',20);
            $table->string('linkman',20);
            $table->string('phone',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advertisements');
    }
}
