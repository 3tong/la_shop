<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navs', function (Blueprint $table) {
            $table->increments('nav_id');
            $table->string('nav_name',50);
			$table->string('url');
			$table->integer('sort_order')->default('0');
            $table->tinyInteger('is_show')->default('0');
			$table->tinyInteger('is_hot')->default('1');
			$table->tinyInteger('is_blank')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('navs');
    }
}
