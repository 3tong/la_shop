<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('role_id');
            $table->string('name')->unique();	
            $table->string('email')->unique();
            $table->string('last_ip', 60);
			$table->string('salt',15);
            $table->string('password', 60);
         	$table->string('create_at',20);
			$table->string('update_at',20);
			$table->string('login_at',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_users');
    }
}
