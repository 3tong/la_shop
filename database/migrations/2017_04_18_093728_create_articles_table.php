<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('art_id');
			$table->integer('cat_id');
            $table->string('title',150);
            $table->string('desc');
            $table->string('img');
			$table->text('content');
			$table->string('editor',50);
			$table->string('phone',50);
			$table->string('wechat',50);
			$table->string('qq',50);
			$table->tinyInteger('is_hot')->default('1');
            $table->tinyInteger('is_show')->default('0');
			$table->tinyInteger('is_top')->default('1');
            $table->integer('sort_order')->default('0');
			$table->integer('click')->default('0');
			$table->string('keyword');
         	$table->string('create_at',20);
			$table->string('update_at',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
