<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('com_id');
			$table->string('com_name');
			$table->integer('user_id');
			$table->string('business_license');
			$table->integer('province_id');
            $table->integer('city_id');
			$table->integer('area_id');
			$table->tinyInteger('type')->default('0');
			$table->tinyInteger('status')->default('0');
			$table->string('linkman',20);
			$table->string('telephone');
			$table->string('address');
			$table->string('desc');
            $table->string('add_time',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
