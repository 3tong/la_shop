$(function(){
    var b_name = navigator.appName;  
    var b_version = navigator.appVersion;  
    var version = b_version.split(";");    
	var trim_version = version[1].replace(/[ ]/g, "");  
    if(b_name == "Microsoft Internet Explorer")
    {  
        /*如果是IE6或者IE7*/  
        if(trim_version == "MSIE7.0" || trim_version == "MSIE6.0")
        {  
			removeAllChild(document.getElementsByTagName('body')[0]);
			var oP = document.getElementsByTagName('body')[0];
			oP.style.lineHeight = '500px';
			oP.style.textAlign = 'center';
			oP.style.fontSize  = '16px';
			var oT = document.createTextNode("———— 系统不支持 IE8.0 以下版本 , 请升级您的浏览器 ————");  
			oP.appendChild(oT);  
			document.body[0].appendChild(oP);  
        }  
    }
    
	function removeAllChild(_elems)  
	{  
	    var nodes = _elems;  
	    while(nodes.hasChildNodes())
	    {  
	        nodes.removeChild(nodes.firstChild);  
	    }  
	}   	
	
	
	//左边菜单
	var menut = $('.main_left .menu li h3');
	menut.click(function(){
		if($(this).next().css('display') == 'block')
		{
			$(this).removeClass('on').addClass('off');
			$(this).next().slideToggle(200);
		}
		else
		{
		    $(this).removeClass('off').addClass('on');
			$(this).next().slideToggle(200);	
		}
	});
	//左边菜单
	


	$('input[type=file].file').live('change',function(){
		var rightFileType = new Array("jpg","png","jpeg");  
        var fileType = $(this).val().substring($(this).val().lastIndexOf(".") + 1);  
        if (!in_array(fileType,rightFileType))
        {
            alert("只支持格式为jpg,png,jpeg格式的图片！");  
            $(this).val('');
        }  
	});
	
	
	$('input[type=file].excel').live('change',function(){
		var rightFileType = new Array("xls","xlsx","csv");  
        var fileType = $(this).val().substring($(this).val().lastIndexOf(".") + 1);  
        if (!in_array(fileType,rightFileType))
        {
            alert("只支持格式为xls,xlsx,csv格式的表格！");  
            $(this).val('');
        }  
	});


  
	function in_array(needle, haystack) {  
	    // 得到needle的类型  
	    var type = typeof needle;  
	    if(type == 'string' || type =='number') {  
	        for(var i in haystack) {  
	            if(haystack[i] == needle) {  
	                return true;  
	            }  
	        }  
	    }  
	    return false;  
	}  



	
});

